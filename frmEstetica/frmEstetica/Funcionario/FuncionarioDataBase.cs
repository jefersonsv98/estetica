﻿    using frmEstetica.Base;
using frmEstetica.Login;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Funcionario
{
    class FuncionarioDataBase
    {
        public FuncionarioDTO Logar(string email, string senha)
        {
            string script =
                              @"SELECT *  
                              FROM tb_funcionario 
                              where ds_email = @ds_email and
                                           nu_senha = @nu_senha";

            List<MySqlParameter> parametros = new List<MySqlParameter>();
            parametros.Add(new MySqlParameter("ds_email", email));
            parametros.Add(new MySqlParameter("nu_senha", senha));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parametros);

                FuncionarioDTO funcionario = null;

                if (reader.Read())
                {
                    funcionario = new FuncionarioDTO();
                    funcionario.Id = reader.GetInt32("id_funcionario");
                funcionario.email = reader.GetString("ds_email");
                funcionario.senha = reader.GetString("nu_senha");
                funcionario.funcionario = reader.GetString("nm_funcionario");
                funcionario.rg = reader.GetString("nu_rg");
                funcionario.cpf = reader.GetString("nu_cpf");
                funcionario.cargo = reader.GetString("ds_cargo");
                funcionario.nascimento = reader.GetDateTime("dt_nascimento");
                funcionario.tel = reader.GetString("nu_tel");
                funcionario.celular = reader.GetString("nu_celular");
                funcionario.rua = reader.GetString("nm_rua");
                funcionario.bairro = reader.GetString("nm_bairro");
                funcionario.numero = reader.GetString("nu_numero");
                funcionario.cep = reader.GetString("nu_cep");
                funcionario.sexo = reader.GetString("ds_sexo");
                funcionario.admissao = reader.GetDateTime("dt_admissao");
                funcionario.PermissaoProduto = reader.GetBoolean("pr_produto");
                funcionario.PermissaoCliente = reader.GetBoolean("pr_cliente");
                funcionario.PermissaoEstoque = reader.GetBoolean("pr_estoque");
                funcionario.PermissaoPedido = reader.GetBoolean("pr_pedido");
                funcionario.PermissaoFolhaPagamento = reader.GetBoolean("pr_folha_pagamento");
                funcionario.PermissaoFornecedor = reader.GetBoolean("pr_fornecedor");
                funcionario.PermissaoFuncionario = reader.GetBoolean("pr_funcionario");
                funcionario.PermissaoFluxoCaixa = reader.GetBoolean("pr_fluxo_caixa");
                funcionario.PermissaoAgendamento = reader.GetBoolean("pr_agendamento");
                }
            reader.Close();

            return funcionario;

        }

        public int Salvar(FuncionarioDTO dados)
        {
            string script =
             @"INSERT INTO tb_funcionario (ds_email, nu_senha, nm_funcionario, nu_rg , nu_cpf , ds_cargo, dt_nascimento, nu_tel, nu_celular, nm_rua, nm_bairro, nu_numero, nu_cep, ds_sexo, dt_admissao, pr_produto, pr_cliente, pr_estoque, pr_pedido, pr_folha_pagamento, pr_fornecedor, pr_funcionario, pr_fluxo_caixa,pr_agendamento)
                          VALUES (@ds_email, @nu_senha, @nm_funcionario, @nu_rg , @nu_cpf , @ds_cargo, @dt_nascimento, @nu_tel, @nu_celular, @nm_rua, @nm_bairro, @nu_numero, @nu_cep, @ds_sexo, @dt_admissao, @pr_produto, @pr_cliente, @pr_estoque, @pr_pedido, @pr_folha_pagamento, @pr_fornecedor, @pr_funcionario, @pr_fluxo_caixa, @pr_agendamento)";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("ds_email", dados.email));
            parms.Add(new MySqlParameter("nu_senha", dados.senha));
            parms.Add(new MySqlParameter("nm_funcionario", dados.funcionario));
            parms.Add(new MySqlParameter("nu_rg", dados.rg));
            parms.Add(new MySqlParameter("nu_cpf", dados.cpf));
            parms.Add(new MySqlParameter("ds_cargo", dados.cargo));
            parms.Add(new MySqlParameter("dt_nascimento", dados.nascimento));
            parms.Add(new MySqlParameter("nu_tel", dados.tel));
            parms.Add(new MySqlParameter("nu_celular", dados.celular));
            parms.Add(new MySqlParameter("nm_rua", dados.rua));
            parms.Add(new MySqlParameter("nm_bairro", dados.bairro));
            parms.Add(new MySqlParameter("nu_numero", dados.numero));
            parms.Add(new MySqlParameter("nu_cep", dados.cep));
            parms.Add(new MySqlParameter("ds_sexo", dados.sexo));
            parms.Add(new MySqlParameter("dt_admissao", dados.admissao));
            parms.Add(new MySqlParameter("pr_produto", dados.PermissaoProduto));
            parms.Add(new MySqlParameter("pr_cliente", dados.PermissaoCliente));
            parms.Add(new MySqlParameter("pr_estoque", dados.PermissaoEstoque));
            parms.Add(new MySqlParameter("pr_pedido", dados.PermissaoPedido));
            parms.Add(new MySqlParameter("pr_folha_pagamento", dados.PermissaoFolhaPagamento));
            parms.Add(new MySqlParameter("pr_fornecedor", dados.PermissaoFornecedor));
            parms.Add(new MySqlParameter("pr_funcionario", dados.PermissaoFuncionario));
            parms.Add(new MySqlParameter("pr_fluxo_caixa", dados.PermissaoFluxoCaixa));
            parms.Add(new MySqlParameter("pr_agendamento", dados.PermissaoAgendamento));

            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<FuncionarioDTO> Consultar(string Nome)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE nm_funcionario like @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", Nome + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.email = reader.GetString("ds_email");
                dto.senha = reader.GetString("nu_senha");
                dto.funcionario = reader.GetString("nm_funcionario");
                dto.rg = reader.GetString("nu_rg");
                dto.cpf = reader.GetString("nu_cpf");
                dto.cargo = reader.GetString("ds_cargo");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.tel = reader.GetString("nu_tel");
                dto.celular = reader.GetString("nu_celular");
                dto.rua = reader.GetString("nm_rua");
                dto.bairro = reader.GetString("nm_bairro");
                dto.numero = reader.GetString("nu_numero");
                dto.cep = reader.GetString("nu_cep");
                dto.sexo = reader.GetString("ds_sexo");
                dto.admissao = reader.GetDateTime("dt_admissao");
                dto.PermissaoProduto = reader.GetBoolean("pr_produto");
                dto.PermissaoCliente = reader.GetBoolean("pr_cliente");
                dto.PermissaoEstoque = reader.GetBoolean("pr_estoque");
                dto.PermissaoPedido = reader.GetBoolean("pr_pedido");
                dto.PermissaoFolhaPagamento = reader.GetBoolean("pr_folha_pagamento");
                dto.PermissaoFornecedor = reader.GetBoolean("pr_fornecedor");
                dto.PermissaoFuncionario = reader.GetBoolean("pr_funcionario");
                dto.PermissaoFluxoCaixa = reader.GetBoolean("pr_fluxo_caixa");
                dto.PermissaoAgendamento = reader.GetBoolean("pr_agendamento");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(FuncionarioDTO dados)
        {
            string script = @"UPDATE tb_funcionario 
                                 SET ds_email  = @ds_email,
                                     nu_senha    = @nu_senha,
                                     nm_funcionario = @nm_funcionario,
                                     nu_rg    = @nu_rg,
                                     nu_cpf = @nu_cpf,
                                     ds_cargo    = @ds_cargo,
                                     dt_nascimento = @dt_nascimento,
                                     nu_tel    = @nu_tel,
                                     nu_celular = @nu_celular,
                                     nm_rua    = @nm_rua,
                                     nm_bairro = @nm_bairro,
                                     nu_numero    = @nu_numero,
                                     nu_cep = @nu_cep,
                                     ds_sexo    = @ds_sexo,
                                     dt_admissao = @dt_admissao,
                                     pr_produto   = @pr_produto,
                                     pr_cliente = @pr_cliente,
                                     pr_Estoque    = @pr_estoque,
                                     pr_pedido = @pr_pedido,
                                     pr_folha_pagamento = @pr_folha_pagamento,
                                     pr_fornecedor = @pr_fornecedor,
                                     pr_funcionario = @pr_funcionario,
                                     pr_fluxo_caixa = @pr_fluxo_caixa,
                                     pr_agendamento = @pr_agendamento
                               WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dados.Id));
            parms.Add(new MySqlParameter("ds_email", dados.email));
            parms.Add(new MySqlParameter("nu_senha", dados.senha));
            parms.Add(new MySqlParameter("nm_funcionario", dados.funcionario));
            parms.Add(new MySqlParameter("nu_rg", dados.rg));
            parms.Add(new MySqlParameter("nu_cpf", dados.cpf));
            parms.Add(new MySqlParameter("ds_cargo", dados.cargo));
            parms.Add(new MySqlParameter("dt_nascimento", dados.nascimento));
            parms.Add(new MySqlParameter("nu_tel", dados.tel));
            parms.Add(new MySqlParameter("nu_celular", dados.celular));
            parms.Add(new MySqlParameter("nm_rua", dados.rua));
            parms.Add(new MySqlParameter("nm_bairro", dados.bairro));
            parms.Add(new MySqlParameter("nu_numero", dados.numero));
            parms.Add(new MySqlParameter("nu_cep", dados.cep));
            parms.Add(new MySqlParameter("ds_sexo", dados.sexo));
            parms.Add(new MySqlParameter("dt_admissao", dados.admissao));
            parms.Add(new MySqlParameter("pr_produto", dados.PermissaoProduto));
            parms.Add(new MySqlParameter("pr_cliente", dados.PermissaoCliente));
            parms.Add(new MySqlParameter("pr_estoque", dados.PermissaoEstoque));
            parms.Add(new MySqlParameter("pr_pedido", dados.PermissaoPedido));
            parms.Add(new MySqlParameter("pr_folha_pagamento", dados.PermissaoFolhaPagamento));
            parms.Add(new MySqlParameter("pr_fornecedor", dados.PermissaoFornecedor));
            parms.Add(new MySqlParameter("pr_funcionario", dados.PermissaoFuncionario));
            parms.Add(new MySqlParameter("pr_fluxo_caixa", dados.PermissaoFluxoCaixa));
            parms.Add(new MySqlParameter("pr_agendamento", dados.PermissaoAgendamento));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.email = reader.GetString("ds_email");
                dto.senha = reader.GetString("nu_senha");
                dto.funcionario = reader.GetString("nm_funcionario");
                dto.rg = reader.GetString("nu_rg");
                dto.cpf = reader.GetString("nu_cpf");
                dto.cargo = reader.GetString("ds_cargo");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.tel = reader.GetString("nu_tel");
                dto.celular = reader.GetString("nu_celular");
                dto.rua = reader.GetString("nm_rua");
                dto.bairro = reader.GetString("nm_bairro");
                dto.numero = reader.GetString("nu_numero");
                dto.cep = reader.GetString("nu_cep");
                dto.sexo = reader.GetString("ds_sexo");
                dto.admissao = reader.GetDateTime("dt_admissao");
                dto.PermissaoProduto = reader.GetBoolean("pr_produto");
                dto.PermissaoCliente = reader.GetBoolean("pr_cliente");
                dto.PermissaoEstoque = reader.GetBoolean("pr_estoque");
                dto.PermissaoPedido = reader.GetBoolean("pr_pedido");
                dto.PermissaoFolhaPagamento = reader.GetBoolean("pr_folha_pagamento");
                dto.PermissaoFornecedor = reader.GetBoolean("pr_fornecedor");
                dto.PermissaoFuncionario = reader.GetBoolean("pr_funcionario");
                dto.PermissaoFluxoCaixa = reader.GetBoolean("pr_fluxo_caixa");
                dto.PermissaoAgendamento = reader.GetBoolean("pr_agendamento");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public FuncionarioDTO ListarPorId(int idProduto)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", idProduto));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO dto = new FuncionarioDTO();
            if (reader.Read())
            {
                dto.Id = reader.GetInt32("id_funcionario");
                dto.email = reader.GetString("ds_email");
                dto.senha = reader.GetString("nu_senha");
                dto.funcionario = reader.GetString("nm_funcionario");
                dto.rg = reader.GetString("nu_rg");
                dto.cpf = reader.GetString("nu_cpf");
                dto.cargo = reader.GetString("ds_cargo");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.tel = reader.GetString("nu_tel");
                dto.celular = reader.GetString("nu_celular");
                dto.rua = reader.GetString("nm_rua");
                dto.bairro = reader.GetString("nm_bairro");
                dto.numero = reader.GetString("nu_numero");
                dto.cep = reader.GetString("nu_cep");
                dto.sexo = reader.GetString("ds_sexo");
                dto.admissao = reader.GetDateTime("dt_admissao");
                dto.PermissaoProduto = reader.GetBoolean("pr_produto");
                dto.PermissaoCliente = reader.GetBoolean("pr_cliente");
                dto.PermissaoEstoque = reader.GetBoolean("pr_estoque");
                dto.PermissaoPedido = reader.GetBoolean("pr_pedido");
                dto.PermissaoFolhaPagamento = reader.GetBoolean("pr_folha_pagamento");
                dto.PermissaoFornecedor = reader.GetBoolean("pr_fornecedor");
                dto.PermissaoFuncionario = reader.GetBoolean("pr_funcionario");
                dto.PermissaoFluxoCaixa = reader.GetBoolean("pr_fluxo_caixa");
                dto.PermissaoAgendamento = reader.GetBoolean("pr_agendamento");
            }
            reader.Close();
            return dto;
        }
    }
}





