﻿using Blibioteca.Developers.Validacao;
using frmEstetica.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica.Funcionario
{
    class FuncionarioBusiness
    {
        public FuncionarioDTO Logar(string email, string senha)
        {

            if (email == string.Empty)
            {
                throw new ArgumentException("Digite o email");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Digite a senha");
            }

            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Logar(email, senha);
        }


        public int Salvar(FuncionarioDTO dto)
        {
            CPF validar = new CPF();
            validar.ValidarCPF(dto.cpf);

            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Salvar(dto);

            if (dto.cpf == string.Empty)
            {
                MessageBox.Show("Digite os numeros do CPF.");
            }
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Consultar(nome);
        }

        public void Remover(int id)
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            db.Remover(id);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Listar();
        }


        public void Alterar(FuncionarioDTO dados)
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            db.Alterar(dados);
        }
        public FuncionarioDTO ListarPorId(int idProduto)
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.ListarPorId(idProduto);
        }
    }
  
}

