﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Funcionario
{
   public class FuncionarioDTO
    {
        public int IdFuncionario { get; set; }

        public int Id { get; set; }
        public string email { get; set; }
        public string senha { get; set; }
        public string funcionario { get; set; }
        public string rg { get; set; }
        public string cpf { get; set; }
        public string cargo { get; set; }
        public DateTime nascimento { get; set; }
        public string tel { get; set; }
        public string celular { get; set; }
        public string rua { get; set; }
        public string bairro { get; set; }
        public string numero { get; set; }
        public string cep { get; set; }
        public string sexo { get; set; }
        public DateTime admissao { get; set; }
        public bool PermissaoProduto { get; set; }
        public bool PermissaoCliente { get; set; }
        public bool PermissaoEstoque { get; set; }
        public bool PermissaoPedido { get; set; }
        public bool PermissaoFolhaPagamento { get; set; }
        public bool PermissaoFornecedor { get; set; }
        public bool PermissaoFuncionario { get; set; }
        public bool PermissaoFluxoCaixa { get; set; }
        public bool PermissaoAgendamento { get; set; }
    }
}
