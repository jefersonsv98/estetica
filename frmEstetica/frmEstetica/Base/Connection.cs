﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Base
{
   public  class Connection
    {
        public MySqlConnection Create()
        {       
            //string connectionString = "server=localhost;database=estetica;uid=root;password=1234";
            //string connectionString = "server=192.168.0.100;database=estetica;uid=nsf;password=nsf@2018;sslmode=none";
            string connectionString = "server=70.37.57.127;database=estetica;uid=nsf;password=nsf@2018;sslmode=none";
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            return connection;
        }
    }
}
