﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Pedido_Compra
{
    class PedidoCompraViewConsultar
    {
        public int Id { get; set; }
        public int QtdProduto { get; set; }
        public string Fornecedor { get; set; }
        public string Produto { get; set; }
        public DateTime DtPedido { get; set; }
    }

}