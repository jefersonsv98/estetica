﻿using frmEstetica.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Pedido_Compra
{
    class PedidoCompraDataBase
    {


        public int Salvar(PedidoCompraDTO dados)
        {
            string script =
             @"INSERT INTO tb_pedido_compra (qtd_produto, tb_fornecedor_id_fornecedor, tb_produto_id_produto, dt_pedido)
                                     VALUES (@qtd_produto, @tb_fornecedor_id_fornecedor, @tb_produto_id_produto, @dt_pedido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qtd_produto", dados.QtdProduto));
            parms.Add(new MySqlParameter("tb_fornecedor_id_fornecedor", dados.FornecedorId));
            parms.Add(new MySqlParameter("tb_produto_id_produto", dados.ProdutoId));
            parms.Add(new MySqlParameter("dt_pedido", dados.DtPedido));

            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<PedidoCompraDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoCompraDTO> lista = new List<PedidoCompraDTO>();
            while (reader.Read())
            {
                PedidoCompraDTO dados = new PedidoCompraDTO();
                dados.Id = reader.GetInt32("id_pedido_compra");
                dados.QtdProduto = reader.GetInt32("qtd_produto");
                dados.FornecedorId = reader.GetInt32("tb_fornecedor_id_fornecedor");
                dados.ProdutoId = reader.GetInt32("tb_produto_id_produto");
                dados.DtPedido = reader.GetDateTime("dt_pedido");

                lista.Add(dados);
            }
            reader.Close();

            return lista;
        }

        public List<PedidoCompraViewConsultar> Consultar(string empresa)
        {
            string script = @"SELECT * FROM vw_pedido_compra_consultar WHERE nm_empresa like @nm_empresa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_empresa", empresa + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoCompraViewConsultar> lista = new List<PedidoCompraViewConsultar>();
            while (reader.Read())
            {
                PedidoCompraViewConsultar dto = new PedidoCompraViewConsultar();
                dto.Id = reader.GetInt32("id_pedido_compra");
                dto.QtdProduto = reader.GetInt32("qtd_produto");
                dto.Fornecedor = reader.GetString("nm_empresa");
                dto.Produto = reader.GetString("ds_produto");
                dto.DtPedido = reader.GetDateTime("dt_pedido");



                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_pedido_compra WHERE id_pedido_compra = @id_pedido_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido_compra", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(PedidoCompraDTO dados)
        {
            string script = @"UPDATE tb_pedido_compra
                                 SET qtd_produto = @qtd_produto,
                                     tb_fornecedor_id_fornecedor = @tb_fornecedor_id_fornecedor,
                                     tb_produto_id_produto = @tb_produto_id_produto,
                                     dt_pedido = @dt_pedido
                               WHERE id_pedido_compra = @id_pedido_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido_compra", dados.Id));
            parms.Add(new MySqlParameter("qtd_produto", dados.QtdProduto));
            parms.Add(new MySqlParameter("tb_fornecedor_id_fornecedor", dados.FornecedorId));
            parms.Add(new MySqlParameter("tb_produto_id_produto", dados.ProdutoId));
            parms.Add(new MySqlParameter("dt_pedido", dados.DtPedido));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }
        public PedidoCompraDTO ListarPorId(int idCompra)
        {
            string script = @"SELECT * FROM tb_pedido_compra WHERE id_pedido_compra = @id_pedido_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido_compra", idCompra));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            PedidoCompraDTO dto = new PedidoCompraDTO();
            if (reader.Read())
            {
                dto.Id = reader.GetInt32("id_pedido_compra");
                dto.QtdProduto = reader.GetInt32("qtd_produto");
                dto.FornecedorId = reader.GetInt32("tb_fornecedor_id_fornecedor");
                dto.ProdutoId = reader.GetInt32("tb_produto_id_produto");
                dto.DtPedido = reader.GetDateTime("dt_pedido");
            }
            reader.Close();
            return dto;
        }
    }
}
