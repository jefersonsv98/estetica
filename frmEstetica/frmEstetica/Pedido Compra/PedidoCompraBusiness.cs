﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Pedido_Compra
{
    class PedidoCompraBusiness
    {
        public int Salvar(PedidoCompraDTO dto)
        {
            PedidoCompraDataBase db = new PedidoCompraDataBase();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            PedidoCompraDataBase db = new PedidoCompraDataBase();
            db.Remover(id);
        }

        public void Alterar(PedidoCompraDTO dados)
        {
            PedidoCompraDataBase db = new PedidoCompraDataBase();
            db.Alterar(dados);
        }

        public List<PedidoCompraViewConsultar> Consultar(string empresa)
        {
            PedidoCompraDataBase db = new PedidoCompraDataBase();
            return db.Consultar(empresa);
        }

        public List<PedidoCompraDTO> Listar()
        {
            PedidoCompraDataBase db = new PedidoCompraDataBase();
            return db.Listar();
        }
        public PedidoCompraDTO ListarPorId(int idCompra)
        {
            PedidoCompraDataBase db = new PedidoCompraDataBase();
            return db.ListarPorId(idCompra);
        }
    }
}