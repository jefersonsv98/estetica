﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Fornecedor
{
    class FornecedorConsultarView
    {
        public int Id { get; set; }
        public string empresa { get; set; }
        public string cnpj { get; set; }
        public string inscricao { get; set; }
        public string rua { get; set; }
        public string cidade { get; set; }
        public string bairro { get; set; }
        public string local { get; set; }
        public string cep { get; set; }
        public string estado { get; set; }
        public string uf { get; set; }
        public string tel { get; set; }
        public string email { get; set; }
        public string Funcionario { get; set; }
    }
}
