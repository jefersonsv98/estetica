﻿using frmEstetica.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Fornecedor
{
    class FornecedorDataBase
    {
        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_fornecedor (nm_empresa,nu_cnpj,nu_inscricao, nm_rua,nm_bairro, nu_local,nu_cep,nm_estado,ds_uf,nu_tel,ds_email, tb_funcionario_id_funcionario)
                                              VALUES (@nm_empresa,@nu_cnpj,@nu_inscricao, @nm_rua,@nm_bairro, @nu_local,@nu_cep,@nm_estado,@ds_uf,@nu_tel,@ds_email, @tb_funcionario_id_funcionario)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dto.funcionarioId));
            parms.Add(new MySqlParameter("nm_empresa", dto.empresa));
            parms.Add(new MySqlParameter("nu_cnpj", dto.cnpj));
            parms.Add(new MySqlParameter("nu_inscricao", dto.inscricao));
            parms.Add(new MySqlParameter("nm_rua", dto.rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.bairro));
            parms.Add(new MySqlParameter("nu_local", dto.local));
            parms.Add(new MySqlParameter("nu_cep", dto.cep));
            parms.Add(new MySqlParameter("nm_estado", dto.estado));
            parms.Add(new MySqlParameter("ds_uf", dto.uf));
            parms.Add(new MySqlParameter("nu_tel", dto.tel));
            parms.Add(new MySqlParameter("ds_email", dto.email));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor
                                 SET nm_empresa = @nm_empresa,
                                     nu_cnpj = @nu_cnpj,
                                     nu_inscricao = @nu_inscricao,
                                     nm_rua  = @nm_rua,
                                     nm_bairro = @nm_bairro,
                                     nu_local = @nu_local,
                                     nu_cep = @nu_cep,
                                     nm_estado    = @nm_estado,
                                     ds_uf = @ds_uf,
                                     nu_tel = @nu_tel,
                                     ds_email = @ds_email
                               WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.Id));
            parms.Add(new MySqlParameter("nm_empresa", dto.empresa));
            parms.Add(new MySqlParameter("nu_cnpj", dto.cnpj));
            parms.Add(new MySqlParameter("nu_inscricao", dto.inscricao));
            parms.Add(new MySqlParameter("nm_rua", dto.rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.bairro));
            parms.Add(new MySqlParameter("nu_local", dto.local));
            parms.Add(new MySqlParameter("nu_cep", dto.cep));
            parms.Add(new MySqlParameter("nm_estado", dto.estado));
            parms.Add(new MySqlParameter("ds_uf", dto.uf));
            parms.Add(new MySqlParameter("nu_tel", dto.tel));
            parms.Add(new MySqlParameter("ds_email", dto.email));

            DataBase db = new DataBase();
            db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FornecedorConsultarView> Consultar(string fornecedor)
        {
            string script = @"SELECT * FROM vw_fornecedor_consultar WHERE nm_empresa like @nm_empresa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_empresa", fornecedor + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorConsultarView> lista = new List<FornecedorConsultarView>();
            while (reader.Read())
            {
                FornecedorConsultarView dto = new FornecedorConsultarView();
                dto.Id = reader.GetInt32("id_fornecedor");
                dto.empresa = reader.GetString("nm_empresa");
                dto.cnpj = reader.GetString("nu_cnpj");
                dto.inscricao = reader.GetString("nu_inscricao");
                dto.rua = reader.GetString("nm_rua");
                dto.bairro = reader.GetString("nm_bairro");
                dto.local = reader.GetString("nu_local");
                dto.cep = reader.GetString("nu_cep");
                dto.estado = reader.GetString("nm_estado");
                dto.uf = reader.GetString("ds_uf");
                dto.tel = reader.GetString("nu_tel");
                dto.email = reader.GetString("ds_email");
                dto.Funcionario = reader.GetString("nm_funcionario");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id = reader.GetInt32("id_fornecedor");
                dto.empresa = reader.GetString("nm_empresa");
                dto.cnpj = reader.GetString("nu_cnpj");
                dto.inscricao = reader.GetString("nu_inscricao");
                dto.rua = reader.GetString("nm_rua");
                dto.bairro = reader.GetString("nm_bairro");
                dto.local = reader.GetString("nu_local");
                dto.cep = reader.GetString("nu_cep");
                dto.estado = reader.GetString("nm_estado");
                dto.uf = reader.GetString("ds_uf");
                dto.tel = reader.GetString("nu_tel");
                dto.email = reader.GetString("ds_email");
                dto.funcionarioId = reader.GetInt32("tb_funcionario_id_funcionario");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
       
       
    }
}
