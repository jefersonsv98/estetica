﻿using Blibioteca.Developers.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Fornecedor
{
    class FornecedorBusiness
    {

        public int Salvar(FornecedorDTO dto)
        {
            CNPJ validar = new CNPJ();
            validar.ValidarCNPJ(dto.cnpj);

            FornecedorDataBase db = new FornecedorDataBase();
            return db.Salvar(dto);
        }

        public void Alterar(FornecedorDTO dto)
        {
            FornecedorDataBase db = new FornecedorDataBase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            FornecedorDataBase db = new FornecedorDataBase();
            db.Remover(id);
        }

        public List<FornecedorConsultarView> Consultar(string Nome)
        {
            FornecedorDataBase db = new FornecedorDataBase();
            return db.Consultar(Nome);
        }

        public List<FornecedorDTO> Listar()
        {
            FornecedorDataBase db = new FornecedorDataBase();
            return db.Listar();
        }
  
    }
}

