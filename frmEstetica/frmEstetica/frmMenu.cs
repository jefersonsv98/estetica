﻿using frmEstetica.Telas.Agendamento;
using frmEstetica.Telas.Cliente;
using frmEstetica.Telas.Folha_de_Pagamento;
using frmEstetica.Telas.Fornecedor;
using frmEstetica.Telas.Pedido;
using frmEstetica.Telas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
            VerificarPermissoes();
        }
        void VerificarPermissoes()
        {
            if (UserSession.UsuarioLogado.PermissaoProduto == false)
            {
                produtoToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoCliente == false)
            {
                clienteToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoEstoque == false)
            {
                estoqueToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoPedido == false)
            {
                pedidoToolStripMenuItem1.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoFolhaPagamento == false)
            {
                folhaDePagamentoToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoFornecedor == false)
            {
                fornecedorToolStripMenuItem1.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoFluxoCaixa == false)
            {
                fluxoDeCaixaToolStripMenuItem1.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoFuncionario == false)
            {
                funcionarioToolStripMenuItem.Enabled = false;
            }
            if (UserSession.UsuarioLogado.PermissaoAgendamento == false)
            {
                agendamentoToolStripMenuItem.Enabled = false;
            }
        }
            private void logoufToolStripMenuItem_Click(object sender, EventArgs e)
            {
              frmLogar retroceder = new frmLogar();
               this.Hide();
               retroceder.ShowDialog();
            }

        private void minimizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void fecharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmEstoque ir = new frmEstoque();
            this.Hide();
            ir.ShowDialog();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            frmCadastrarFornecedor ir = new frmCadastrarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
        }

        private void button13_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario ir = new frmCadastrarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmCadastrarCliente ir = new frmCadastrarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            frmConsultarCliente ir = new frmConsultarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmCadastrarPedido ir = new frmCadastrarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            frmConsultarPedido ir = new frmConsultarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            frmFluxodeCaixa ir = new frmFluxodeCaixa();
            this.Hide();
            ir.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmCadastrarProduto ir = new frmCadastrarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            frmConsultarProduto ir = new frmConsultarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor ir = new frmConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario ir = new frmConsultarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void button17_Click(object sender, EventArgs e)
        {
        }

        private void button18_Click(object sender, EventArgs e)
        {
        }

        private void button15_Click(object sender, EventArgs e)
        {
            frmAgendamento ir = new frmAgendamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            frmFolhadePagamento ir = new frmFolhadePagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void pedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarProduto ir = new frmCadastrarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarProduto ir = new frmConsultarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarCliente ir = new frmCadastrarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmConsultarCliente ir = new frmConsultarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmEstoque ir = new frmEstoque();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmEstoque ir = new frmEstoque();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmCadastrarPedido ir = new frmCadastrarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmConsultarPedido ir = new frmConsultarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmFolhadePagamento ir = new frmFolhadePagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmConsultarFolhadePagamento ir = new frmConsultarFolhadePagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmCadastrarFornecedor ir = new frmCadastrarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor ir = new frmConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            frmFluxodeCaixa ir = new frmFluxodeCaixa();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario ir = new frmCadastrarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario ir = new frmConsultarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void agendamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void agendarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAgendamento ir = new frmAgendamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void cancelarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterarAgendamento ir = new frmAlterarAgendamento();
              this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            frmConsultarAgendamento ir = new frmConsultarAgendamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            frmAlterarFuncionario ir = new frmAlterarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            frmFolhadePagamento ir = new frmFolhadePagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmAlterarPagamento ir = new frmAlterarPagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmAlterarFornecedor ir = new frmAlterarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            frmAlterarProduto ir = new frmAlterarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void pedirAoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void cadastrarToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            frmCadastrarPedido ir = new frmCadastrarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            frmConsultarPedido ir = new frmConsultarPedido();
            this.Hide();
            ir.ShowDialog();


        }

        private void pedirAoFornecedorToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmPedirFornecedor ir = new frmPedirFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            frmAlterarCliente ir = new frmAlterarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void realizarPedidoAoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedirFornecedor ir = new frmPedirFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarPedidoRealizoComOFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterarPedidoFornecedor ir = new frmAlterarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarPedidoRealizadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarPedidoFornecedor ir = new frmConsultarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmAlterarPedido ir = new frmAlterarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void recarregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSplash ir = new frmSplash();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmConsultarProduto ir = new frmConsultarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterarProduto ir = new frmAlterarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmConsultarCliente ir = new frmConsultarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmAlterarCliente ir = new frmAlterarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            frmConsultarPedido ir = new frmConsultarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            frmAlterarPedido ir = new frmAlterarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarPedidoAoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarPedidoFornecedor ir = new frmConsultarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarPedidoAoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterarPedidoFornecedor ir = new frmAlterarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem3_Click_2(object sender, EventArgs e)
        {
            frmConsultarFolhadePagamento ir = new frmConsultarFolhadePagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            frmAlterarPagamento ir = new frmAlterarPagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem4_Click_1(object sender, EventArgs e)
        {
            frmConsultarFornecedor ir = new frmConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmAlterarFornecedor ir = new frmAlterarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem5_Click_1(object sender, EventArgs e)
        {
            frmConsultarFuncionario ir = new frmConsultarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem5_Click_1(object sender, EventArgs e)
        {
            frmAlterarFuncionario ir = new frmAlterarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem7_Click_1(object sender, EventArgs e)
        {
            frmConsultarAgendamento ir = new frmConsultarAgendamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem6_Click_1(object sender, EventArgs e)
        {
            frmAlterarAgendamento ir = new frmAlterarAgendamento();
            this.Hide();
            ir.ShowDialog();

        }
    }
}
