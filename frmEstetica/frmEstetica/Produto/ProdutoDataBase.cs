﻿using frmEstetica.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Produto
{
    class ProdutoDataBase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto (ds_produto,vl_preco,qtd_produto)
                                              VALUES (@ds_produto,@vl_preco, @qtd_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_produto", dto.descricaoproduto));
            parms.Add(new MySqlParameter("vl_preco", dto.valorproduto));
            parms.Add(new MySqlParameter("qtd_produto", dto.quantidade));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_produto
                                 SET ds_produto = @ds_produto,
                                     vl_preco = @vl_preco,
                                     qtd_produto = @qtd_produto
                                    WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));
            parms.Add(new MySqlParameter("ds_produto", dto.descricaoproduto));
            parms.Add(new MySqlParameter("vl_preco", dto.valorproduto));
            parms.Add(new MySqlParameter("qtd_produto", dto.quantidade));

            DataBase db = new DataBase();
            db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoDTO> Consultar(string Nome)
        {
            string script = @"SELECT * FROM tb_produto WHERE ds_produto like @ds_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_produto", Nome + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.IdProduto = reader.GetInt32("id_produto");
                dto.descricaoproduto = reader.GetString("ds_produto");
                dto.valorproduto = reader.GetDecimal("vl_preco");
                dto.quantidade = reader.GetInt32("qtd_produto");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.IdProduto = reader.GetInt32("id_produto");
                dto.descricaoproduto = reader.GetString("ds_produto");
                dto.valorproduto = reader.GetDecimal("vl_preco");
                dto.quantidade = reader.GetInt32("qtd_produto");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public ProdutoDTO ListarPorId(int idProduto)
        {
            string script = @"SELECT * FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", idProduto));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ProdutoDTO dto = new ProdutoDTO();
            if (reader.Read())
            {
                dto.IdProduto = reader.GetInt32("id_produto");
                dto.descricaoproduto = reader.GetString("ds_produto");
                dto.valorproduto = reader.GetDecimal("vl_preco");
                dto.quantidade = reader.GetInt32("qtd_produto");
            }
            reader.Close();
            return dto;
        }
    }
}
