﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Salvar(dto);
        }

        public List<ProdutoDTO> Consultar(string nome)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Consultar(nome);
        }

        public void Remover(int id)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            db.Remover(id);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Listar();
        }


        public void Alterar(ProdutoDTO dados)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            db.Alterar(dados);
        }
        public ProdutoDTO ListarPorId(int idProduto)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.ListarPorId(idProduto);
        }
    }
}
