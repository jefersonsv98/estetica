﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Produto
{
    class ProdutoDTO
    {
        public int IdProduto { get; set; }
        public string descricaoproduto { get; set; }
        public decimal valorproduto { get; set; }
        public int quantidade { get; set; }
    }
}
