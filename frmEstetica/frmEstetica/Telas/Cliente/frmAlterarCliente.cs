﻿using frmEstetica.API.Correio;
using frmEstetica.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica.Telas.Cliente
{
    public partial class frmAlterarCliente : Form
    {
        public frmAlterarCliente()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Listar();

            txtId.ValueMember = nameof(ClienteDTO.Id);
            txtId.DisplayMember = nameof(ClienteDTO.Id);
            txtId.DataSource = lista;
        }
        int pkcliente;

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRG_Click(object sender, EventArgs e)
        {

        }

        private void txtcpf_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtrg1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void frmAlterarCliente_Load(object sender, EventArgs e)
        {
            
        }

        private void txtRua_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua1.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;
                

            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void buttonCliente_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            try
            {
                ClienteDTO dto = new ClienteDTO();


                dto.Id = Convert.ToInt32(txtId.Text.Trim());
                dto.nome = txtnome.Text.Trim();
                dto.rg = txtrg1.Text.Trim();
                dto.cpf = txtcpf.Text.Trim();
                dto.sexo = cbosexo.Text.Trim();
                dto.nascimento = Convert.ToDateTime(txtnascimento.Text);
                dto.telefone = txttelefone.Text.Trim();
                dto.celular = txtcelular.Text.Trim();
                dto.rua = txtRua1.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.numero = txtnumero.Text.Trim();


                if (dto.Id == 0)
                {
                    MessageBox.Show("ID é obrigatório.");
                    return;
                }

                if (dto.nome == string.Empty || dto.nome == "Digite seu nome")
                {
                    MessageBox.Show("Digite seu nome");
                    return;
                }

                if (dto.rg == string.Empty || txtrg1.Text.Length <= 11)
                {
                    MessageBox.Show("Digite seu RG!");
                    return;
                }

                if (dto.cpf == string.Empty || dto.cpf == "Digite seu CPF" || txtcpf.Text.Length <= 11)
                {
                    MessageBox.Show("Coloque seu CPF.");
                    return;
                }


                if (dto.sexo == string.Empty)
                {
                    MessageBox.Show("Selecione seu sexo.");
                    return;
                }
                if (dto.telefone == string.Empty || txttelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Telefone é obrigatório.");
                    return;
                }


                if (dto.celular == string.Empty || txtcelular.Text.Length <= 14)
                {
                    MessageBox.Show("Celular é obrigatório.");
                    return;
                }

                if (dto.rua == string.Empty || dto.rua == "Nome da rua")
                {
                    MessageBox.Show("Rua é obrigatória.");
                    return;
                }

                if (dto.bairro == string.Empty || dto.bairro == "Bairro que mora")
                {
                    MessageBox.Show("Bairro é obrigatório.");
                    return;
                }
                if (dto.cep == string.Empty || txtCep.Text.Length <= 8)
                {
                    MessageBox.Show("Digite os numeros do CEP.");
                    return;
                }

                if (dto.numero == string.Empty || dto.numero == "Nº da casa")
                {
                    MessageBox.Show("Numero é obrigatório.");
                    return;
                }

                ClienteBusiness business = new ClienteBusiness();
                business.Alterar(dto);

                MessageBox.Show("Informaçoes alteradas com sucesso.", "Wonderland",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                frmAlterarCliente ir = new frmAlterarCliente();
                this.Hide();
                ir.ShowDialog();
            }
            catch (Exception)
            {

                MessageBox.Show("Dados Invalidos");
                return;
            }

           
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtnome_Click(object sender, EventArgs e)
        {
            if (txtnome.Text == "Nome Completo")
            {
                txtnome.Text = string.Empty;
            }
        }

        private void txtnumero_Click(object sender, EventArgs e)
        {
            if (txtnumero.Text == "Nº da casa")
            {
                txtnumero.Text = string.Empty;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDadosClientes();

        }
        private void CarregarDadosClientes()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Consultar(txtId.Text);
            ClienteDTO dto = lista[0];
            txtcpf.Text = dto.cpf;
            txttelefone.Text = dto.telefone;
            txtCep.Text = dto.cep;
            txtnumero.Text = dto.numero;
            pkcliente = dto.Id;

        }

        private void txtId_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClienteDTO Pro = txtId.SelectedItem as ClienteDTO;

            ClienteBusiness business = new ClienteBusiness();
            ClienteDTO prod = business.ListarPorId(Pro.Id);

            txtnome.Text = prod.nome.ToString();
            txtcpf.Text = prod.cpf.ToString();
            txtrg1.Text = prod.rg.ToString();
            txtcelular.Text = prod.celular.ToString();
            txttelefone.Text = prod.telefone.ToString();
            txtCep.Text = prod.cep.ToString();
            txtnumero.Text = prod.numero.ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            frmAlterarCliente ir = new frmAlterarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmConsultarCliente ir = new frmConsultarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtBairro_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
