﻿using frmEstetica.Cliente;
using frmEstetica.Telas.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmConsultarCliente : Form
    {
        public frmConsultarCliente()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Consultar(txtConsultarCliente.Text.Trim());

            dgvCliente.AutoGenerateColumns = false;
            dgvCliente.DataSource = lista;
        }

        private void dgvCliente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 12)
                {
                    ClienteDTO cat = dgvCliente.CurrentRow.DataBoundItem as ClienteDTO;

                    ClienteBusiness business = new ClienteBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse é um dos produtos que não pode ser removido");
                return;
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void frmConsultarCliente_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAlterarCliente ir = new frmAlterarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmCadastrarCliente ir = new frmCadastrarCliente();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
