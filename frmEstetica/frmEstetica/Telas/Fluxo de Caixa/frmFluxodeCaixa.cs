﻿using frmEstetica.Fluxo_de_Caixa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmFluxodeCaixa : Form
    {
        public frmFluxodeCaixa()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombo2();
        }
        void CarregarCombo()
        {
            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            List<FluxodeCaixaDTO> lista = business.Listar();

            cboId.ValueMember = nameof(FluxodeCaixaConsultarView.Id);
            cboId.DisplayMember = nameof(FluxodeCaixaConsultarView.Id);
            cboId.DataSource = lista;
        }

        void CarregarCombo2()
        {
            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            List<FluxoCaixaDTO2> lista = business.ListarVenda();

            cboIdvenda.ValueMember = nameof(FluxoCaixaConsultarView2.Id);
            cboIdvenda.DisplayMember = nameof(FluxoCaixaConsultarView2.Id);
            cboIdvenda.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
        }

        private void cboId_SelectedIndexChanged(object sender, EventArgs e)
        {
            FluxodeCaixaDTO Pro = cboId.SelectedItem as FluxodeCaixaDTO;


            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            FluxodeCaixaConsultarView prod = business.ListarPorId(Pro.Id);

            lblPagamento.Text = prod.Pagamento.ToString();
            lblExtra1.Text = prod.horaextra1.ToString();
            lblExtra2.Text = prod.horaextra2.ToString();
            lblRefeicao.Text = prod.refeicao.ToString();
            lblTransporte.Text = prod.transporte.ToString();

        }

        private void cboIdvenda_SelectedIndexChanged(object sender, EventArgs e)
        {
            FluxoCaixaDTO2 Pro2 = cboIdvenda.SelectedItem as FluxoCaixaDTO2;

            FluxodeCaixaBusiness business2 = new FluxodeCaixaBusiness();
            FluxoCaixaConsultarView2 prod2 = business2.ListarPorIdVenda(Pro2.Id);

            lblVenda.Text = prod2.Venda.ToString();
        }

        private void lblRefeicao_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void frmFluxodeCaixa_Load(object sender, EventArgs e)
        {
            lblSaldo.Text = Convert.ToString(Convert.ToDecimal(lblPagamento.Text) + Convert.ToDecimal(lblExtra1.Text) + Convert.ToDecimal(lblExtra2.Text) + Convert.ToDecimal(lblRefeicao.Text) + Convert.ToDecimal(lblTransporte.Text));
            lblLucro.Text = Convert.ToString(Convert.ToDecimal(lblVenda.Text) - Convert.ToDecimal(lblSaldo.Text));
        }
    } 
}
