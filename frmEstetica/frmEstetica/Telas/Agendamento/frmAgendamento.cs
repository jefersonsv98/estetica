﻿using frmEstetica.Agendamento;
using frmEstetica.API.Correio;
using frmEstetica.Cliente;
using frmEstetica.Telas.Agendamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmAgendamento : Form
    {
        public frmAgendamento()
        {
            InitializeComponent();
        }
        int pkcliente;

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;
            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }
        private void CarregarDadosCLientes()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Consultar(txtnome.Text);
            ClienteDTO dto = lista[0];
            txtcpf.Text = dto.cpf;
            txttelefone.Text = dto.telefone;
            txtCep.Text = dto.cep;
            txtnumero.Text = dto.numero;
            pkcliente = dto.Id;
        }


        private void button4_Click(object sender, EventArgs e)
        {

            try
            {
                AgendamentoDTO dto = new AgendamentoDTO();

                int pk = UserSession.UsuarioLogado.Id;
                dto.Idfuncionarios = pk;
                dto.nome = txtnome.Text.Trim();
                dto.cpf = txtcpf.Text.Trim();
                dto.telefone = txttelefone.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.numero = txtnumero.Text.Trim();
                dto.rua = txtRua.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.data = Convert.ToDateTime(txtdata.Text);
                dto.hora = (txthora.Text);
                dto.observacao = txtobs.Text.Trim();
                dto.IDcliente = pkcliente;


                if (dto.nome == string.Empty || dto.nome == "Digite seu nome")
                {
                    MessageBox.Show("Digite seu nome");
                    return;
                }
                if (dto.cpf == string.Empty || txtcpf.Text.Length <= 11)
                {
                    MessageBox.Show("Digite os numeros do CPF.");
                    return;
                }
                if (dto.telefone == string.Empty || txttelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Telefone é obrigatório.");
                    return;
                }
                if (dto.cep == string.Empty || txtCep.Text.Length <= 8)
                {
                    MessageBox.Show("Digite os numeros do CEP.");
                    return;
                }
                if (dto.numero == string.Empty || dto.numero == "Nº da casa")
                {
                    MessageBox.Show("Numero é obrigatório.");
                    return;
                }
                if (dto.rua == string.Empty || dto.rua == "Nome da rua")
                {
                    MessageBox.Show("Rua é obrigatória.");
                    return;
                }

                if (dto.bairro == string.Empty || dto.bairro == "Bairro que mora")
                {
                    MessageBox.Show("Bairro é obrigatório.");
                    return;
                }
                if (dto.hora == string.Empty || dto.hora == "Horario é obrigatorio!")
                {
                    MessageBox.Show("Horario é obrigatorio!");
                    return;
                }

                AgendamentoBusiness business = new AgendamentoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Agendamento realizado com sucesso.", "Wonderland",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                frmMenu ir = new frmMenu();
                this.Hide();
                ir.ShowDialog();
            }
            catch (Exception)
            {

                MessageBox.Show("Informações Invalidas!");
            }
           

           




        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            CarregarDadosCLientes();
        }

        private void txtnumero_Click(object sender, EventArgs e)
        {
            if (txtnumero.Text == "Nº da casa")
            {
                txtnumero.Text = string.Empty;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmConsultarAgendamento ir = new frmConsultarAgendamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmAlterarAgendamento ir = new frmAlterarAgendamento();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
