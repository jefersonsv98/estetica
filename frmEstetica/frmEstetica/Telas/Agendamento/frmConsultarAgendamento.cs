﻿using frmEstetica.Agendamento;
using frmEstetica.Telas.Agendamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmConsultarAgendamento : Form
    {
        public frmConsultarAgendamento()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AgendamentoBusiness business = new AgendamentoBusiness();
            List<AgendamentoDTO> lista = business.Consultar(txtConsultarAgendamento.Text.Trim());

            dgvAgendamento.AutoGenerateColumns = false;
            dgvAgendamento.DataSource = lista;
        }

        private void dgvAgendamento_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if (e.ColumnIndex == 11)
                {
                    AgendamentoDTO cat = dgvAgendamento.CurrentRow.DataBoundItem as AgendamentoDTO;

                    AgendamentoBusiness business = new AgendamentoBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse agendamento não pode ser removido no momento");
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAlterarAgendamento ir = new frmAlterarAgendamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAgendamento ir = new frmAgendamento();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
