﻿using frmEstetica.Produto;
using frmEstetica.Telas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmConsultarProduto : Form
    {
        public frmConsultarProduto()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Consultar(txtConsultarProduto.Text.Trim());

            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = lista;
        }

        private void dgvProduto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4)
                {
                    ProdutoDTO cat = dgvProduto.CurrentRow.DataBoundItem as ProdutoDTO;

                    ProdutoBusiness business = new ProdutoBusiness();
                    business.Remover(cat.IdProduto);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse é um dos produtos que não pode ser removido");
                return;
            }

        }

        private void dgvProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCadastrarProduto ir = new frmCadastrarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAlterarProduto ir = new frmAlterarProduto();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
