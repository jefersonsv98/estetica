﻿using frmEstetica.Produto;
using frmEstetica.Telas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmCadastrarProduto : Form
    {
        public frmCadastrarProduto()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {

                ProdutoDTO dto = new ProdutoDTO();

                dto.descricaoproduto = txtdescricaoproduto.Text.Trim();
                dto.valorproduto = Convert.ToDecimal(txtvalor.Text.Trim());
                dto.quantidade = Convert.ToInt32(txtquantidade.Text.Trim());

                if (dto.descricaoproduto == string.Empty || dto.descricaoproduto == "Informe o nome do produto")
                {
                    MessageBox.Show("Produto é obrigatório.");
                    return;
                }

                if (dto.valorproduto == 0 || dto.valorproduto <= 0)
                {
                    MessageBox.Show("Valor é obrigatório.");
                    return;
                }

                if (dto.quantidade == 0 || dto. quantidade <= 0)
                {
                    MessageBox.Show("Quantidade é obrigatório.");
                    return;
                }

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo.", "Wonderland",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                frmCadastrarProduto ir = new frmCadastrarProduto();
                this.Hide();
                ir.ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("Informações invalidas!");
                return;
            }

        }

        private void txtvalor_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtdescricaoproduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void frmCadastrarProduto_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtquantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAlterarProduto ir = new frmAlterarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            frmConsultarProduto ir = new frmConsultarProduto();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
