﻿using frmEstetica.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica.Telas.Produto
{
    public partial class frmAlterarProduto : Form
    {
        public frmAlterarProduto()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            txtId.ValueMember = nameof(ProdutoDTO.IdProduto);
            txtId.DisplayMember = nameof(ProdutoDTO.IdProduto);
            txtId.DataSource = lista;
        }

        private void frmAlterarProduto_Load(object sender, EventArgs e)
        {
        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = new ProdutoDTO();


                dto.IdProduto = Convert.ToInt32(txtId.Text.Trim());
                dto.descricaoproduto = txtdescricaoproduto.Text.Trim();
                dto.valorproduto = Convert.ToDecimal(txtvalor.Text.Trim());
                dto.quantidade = Convert.ToInt32(txtquantidade.Text.Trim());

                if (dto.IdProduto == 0 || dto.IdProduto <= 0)
                {
                    MessageBox.Show("ID é obrigatório.");
                    return;
                }

                if (dto.descricaoproduto == string.Empty || dto.descricaoproduto == "Informe o nome do produto")
                {
                    MessageBox.Show("Produto é obrigatório.");
                    return;
                }

                if (dto.valorproduto == 0 || dto.valorproduto <= 0)
                {
                    MessageBox.Show("Valor é obrigatório.");
                    return;
                }
                if (dto.quantidade == 0 || dto.quantidade <= 0)
                {
                    MessageBox.Show("Quantidade é obrigatório.");
                    return;
                }

                ProdutoDataBase db = new ProdutoDataBase();
                db.Alterar(dto);

                MessageBox.Show("Informação alterada com sucesso.", "Wonderland",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                frmAlterarProduto ir = new frmAlterarProduto();
                this.Hide();
                ir.ShowDialog();

            }
            catch (Exception)
            {
                MessageBox.Show("Tente novamente!");
                return;
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtId_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO Pro = txtId.SelectedItem as ProdutoDTO;

            ProdutoBusiness business = new ProdutoBusiness();
            ProdutoDTO prod = business.ListarPorId(Pro.IdProduto);

            txtdescricaoproduto.Text = prod.descricaoproduto.ToString();
            txtvalor.Text = prod.valorproduto.ToString();
            txtquantidade.Text = prod.quantidade.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCadastrarProduto ir = new frmCadastrarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmConsultarProduto ir = new frmConsultarProduto();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
