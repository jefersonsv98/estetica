﻿using frmEstetica.API.Correio;
using frmEstetica.Fornecedor;
using frmEstetica.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica.Telas.Fornecedor
{
    public partial class frmAlterarFornecedor : Form
    {
        public frmAlterarFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.IdFuncionario);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.funcionario);
            cboFuncionario.DataSource = lista;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;

                FornecedorDTO dto = new FornecedorDTO();
                dto.IdFornecedor = Convert.ToInt32(txtId.Text.Trim());
                dto.funcionarioId = Fun.IdFuncionario;
               // dto.Id = Convert.ToInt32(txtId.Text.Trim());
                dto.empresa = txtnome.Text.Trim();
                dto.cnpj = txtCNPJ.Text.Trim();
                dto.inscricao = txtInscricao.Text.Trim();
                dto.rua = txtRua.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.local = txtNumero.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.estado = txtEstado.Text.Trim();
                dto.uf = txtUF.Text.Trim();
                dto.tel = txtTelefone.Text.Trim();
                dto.email = txtEmail.Text.Trim();



                if (dto.IdFornecedor == 0 || dto.IdFornecedor <=0)
                {
                    MessageBox.Show("Id Invalido");

                    return;
                }

                if (dto.empresa == string.Empty || dto.empresa == "Empresa")
                {
                    MessageBox.Show("Digite o nome da empresa");

                    return;
                }
                if (dto.cnpj == string.Empty || dto.rua == "Digite seu CNPJ" || Text.Length <= 11)
                {
                    MessageBox.Show("Digite seu CNPJ");

                    return;
                }
                if (dto.inscricao == string.Empty || dto.inscricao == "Digite sua inscrição" || Text.Length <= 18)
                {
                    MessageBox.Show("Digite sua inscrição");

                    return;
                }

                if (dto.rua == string.Empty || dto.rua == "Digite sua rua")
                {
                    MessageBox.Show("Digite sua rua");

                    return;
                }

                if (dto.bairro == string.Empty || dto.bairro == "Bairro")
                {
                    MessageBox.Show("Digite seu bairro");

                    return;
                }


                if (dto.local == string.Empty || dto.local == "Digite o numero da sua casa")
                {
                    MessageBox.Show("Digite o numero da sua casa");

                    return;
                }

                if (dto.cep == string.Empty || dto.cep == "Digite seu cep" || txtCep.Text.Length <= 8)
                {
                    MessageBox.Show("Digite o cep");

                    return;
                }
                if (dto.estado == string.Empty || dto.estado == "Digite sua rua")
                {
                    MessageBox.Show("Digite sua rua");

                    return;
                }
                if (dto.uf == string.Empty || dto.uf == "Digite sua rua" || Text.Length <= 2)
                {
                    MessageBox.Show("Digite sua rua");

                    return;
                }

                if (dto.tel == string.Empty || dto.tel == "Digite seu telefone" || Text.Length <= 9)
                {
                    MessageBox.Show("Digite o telefone");

                    return;
                }


                if (dto.email == string.Empty || dto.email == "Digite seu Email")
                {
                    MessageBox.Show("Digite o seu email");

                    return;
                }

                FornecedorBusiness business = new FornecedorBusiness();
                business.Alterar(dto);

                MessageBox.Show("Informaçoes alteradas com sucesso.", "Wonderland", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
             catch (Exception)
            {

                MessageBox.Show("Não foi possivel realizar alteração");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;
                txtEstado.Text = dto.Localidade;
                txtUF.Text = dto.UF;

            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;
                txtEstado.Text = dto.Localidade;
                txtUF.Text = dto.UF;

            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void txtNumero_Click(object sender, EventArgs e)
        {

            if (txtNumero.Text == "Nº")
            {
                txtNumero.Text = string.Empty;
            }
        }

        private void txtId_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor ir = new frmConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmCadastrarFornecedor ir = new frmCadastrarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
