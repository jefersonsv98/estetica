﻿using frmEstetica.API.Correio;
using frmEstetica.Fornecedor;
using frmEstetica.Funcionario;
using frmEstetica.Telas.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmCadastrarFornecedor : Form
    {
        public frmCadastrarFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
        }
        public void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.Id);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.funcionario);
            cboFuncionario.DataSource = lista;
        }

        private void frmCadastrarFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
        }   

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;
                

            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;
                txtEstado.Text = dto.Localidade;
                txtUF.Text = dto.UF;

            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            FuncionarioDTO dto = cboFuncionario.SelectedItem as FuncionarioDTO;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;

                FornecedorDTO dto = new FornecedorDTO();
                dto.funcionarioId = Fun.Id;
                dto.empresa = txtnome.Text.Trim();
                dto.cnpj = txtCNPJ.Text;
                dto.inscricao = txtInscricao.Text.Trim();
                dto.rua = txtRua.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.local = txtNumero.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.estado = txtEstado.Text.Trim();
                dto.uf = txtUF.Text.Trim();
                dto.tel = txtTelefone.Text.Trim();
                dto.email = txtEmail.Text.Trim();

                if (dto.empresa == string.Empty || dto.empresa == "Empresa")
                {
                    MessageBox.Show("Digite o nome da empresa");

                    return;
                }
                if (dto.cnpj == string.Empty || txtCNPJ.Text.Length <= 17)
                {
                    MessageBox.Show("Digite seu CNPJ");

                    return;
                }
                if (dto.inscricao == string.Empty || dto.inscricao == "Digite sua inscrição" || txtInscricao.Text.Length <= 11)
                {
                    MessageBox.Show("Digite sua inscrição");

                    return;
                }

                if (dto.rua == string.Empty || dto.rua == "Digite sua rua")
                {
                    MessageBox.Show("Digite sua rua");

                    return;
                }

                if (dto.bairro == string.Empty || dto.bairro == "Bairro")
                {
                    MessageBox.Show("Digite seu bairro");

                    return;
                }


                if (dto.local == string.Empty || dto.local == "Digite o numero da sua casa")
                {
                    MessageBox.Show("Digite o numero da sua casa");

                    return;
                }

                if (dto.cep == string.Empty || dto.cep == "Digite seu cep" || txtCep.Text.Length <= 8)
                {
                    MessageBox.Show("Digite o cep");

                    return;
                }

                if (dto.estado == string.Empty || dto.estado == "Digite seu estado")
                {
                    MessageBox.Show("Digite sua estado");

                    return;
                }

                if (dto.uf == string.Empty || txtUF.Text.Length <= 1)
                {
                    MessageBox.Show("Digitar uf");

                    return;
                }

                if (dto.tel == string.Empty || dto.tel == "Digite seu telefone" || txtTelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Digite o telefone");

                    return;
                }


                if (dto.email == string.Empty || dto.email == "Digite seu Email")
                {
                    MessageBox.Show("Digite o seu email");

                    return;
                }

                FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(dto);

                MessageBox.Show("Fornecedor salvo com sucesso.", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            { 
                MessageBox.Show("Dados Invalidos");
                return; 
            }
            
        }

        private void txtnome_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtEstado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtNumero_Click(object sender, EventArgs e)
        {

            if (txtNumero.Text == "Nº")
            {
                txtNumero.Text = string.Empty;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor ir = new frmConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            frmAlterarFornecedor ir = new frmAlterarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
   
