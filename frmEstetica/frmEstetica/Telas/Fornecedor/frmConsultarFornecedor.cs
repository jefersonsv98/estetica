﻿using frmEstetica.Fornecedor;
using frmEstetica.Telas.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmConsultarFornecedor : Form
    {
        public frmConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorConsultarView> lista = business.Consultar(txtConsultarFornecedor.Text.Trim());

            dgvFornecedor.AutoGenerateColumns = false;
            dgvFornecedor.DataSource = lista;
        }

        private void dgvFornecedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvFornecedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if (e.ColumnIndex == 13)
                {
                    FornecedorConsultarView cat = dgvFornecedor.CurrentRow.DataBoundItem as FornecedorConsultarView;

                    FornecedorBusiness business = new FornecedorBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse fornecedor não pode ser removido agora");
                frmConsultarFornecedor ir = new frmConsultarFornecedor();
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAlterarFornecedor ir = new frmAlterarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmCadastrarFornecedor ir = new frmCadastrarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
