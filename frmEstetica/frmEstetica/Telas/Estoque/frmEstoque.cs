﻿using frmEstetica.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmEstoque : Form
    {
        public frmEstoque()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista;
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

            ProdutoBusiness business = new ProdutoBusiness();
            ProdutoDTO prod = business.ListarPorId(Pro.IdProduto);

            txtId.Text = prod.IdProduto.ToString();
            txtQuantidade.Text = prod.quantidade.ToString();
            txtvalor.Text = prod.valorproduto.ToString();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Consultar(txtProduto.Text.Trim());

            dgvEstoque.AutoGenerateColumns = false;
            dgvEstoque.DataSource = lista;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

            ProdutoDTO dto = new ProdutoDTO();


            dto.IdProduto = Convert.ToInt32(txtId.Text.Trim());
            dto.descricaoproduto = Convert.ToString(cboProduto.Text.Trim());
            dto.valorproduto = dto.valorproduto = Convert.ToDecimal(txtvalor.Text.Trim());
            dto.quantidade = Convert.ToInt32(txtQuantidade.Text);

            if (dto.IdProduto == 0 || dto.IdProduto <= 0)
            {
                MessageBox.Show("Id Invalido.");
                return;
            }

            ProdutoDataBase business = new ProdutoDataBase();
            business.Alterar(dto);
            MessageBox.Show("Estoque alterado com sucesso.", "Wonderland", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }
    }
}

