﻿using frmEstetica.Fornecedor;
using frmEstetica.Pedido_Compra;
using frmEstetica.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica.Telas.Pedido
{
    public partial class frmPedirFornecedor : Form
    {

        BindingList<FornecedorDTO> Fornecedor = new BindingList<FornecedorDTO>();
        BindingList<ProdutoDTO> Produto = new BindingList<ProdutoDTO>();
        public frmPedirFornecedor()
        {
            InitializeComponent();
            CarregarCombo1();
            CarregarCombo2();
        }

        private void CarregarCombo1()
        {

            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.empresa);
            cboFornecedor.DataSource = lista;
        }
        private void CarregarCombo2()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO Fun = cboFornecedor.SelectedItem as FornecedorDTO;
                ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

                PedidoCompraDTO dto = new PedidoCompraDTO();
                dto.FornecedorId = Fun.Id;
                dto.ProdutoId = Pro.IdProduto;
                dto.DtPedido = Convert.ToDateTime(txtData.Text);
                dto.QtdProduto = Convert.ToInt32(txtQuantidade.Text);

                if (dto.QtdProduto == 0 || dto.QtdProduto <= 0)
                {
                    MessageBox.Show("Quantidade e necessaria");
                    return;
                }

              

                PedidoCompraBusiness business = new PedidoCompraBusiness();
                business.Salvar(dto);
                MessageBox.Show("Pedido ao fornecedor realizado com sucesso.", "Wonderland", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Informações invalidas");
                return;
            }
        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO dto = cboFornecedor.SelectedItem as FornecedorDTO;
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void frmPedirFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void txtData_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAlterarPedidoFornecedor ir = new frmAlterarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmConsultarPedidoFornecedor ir = new frmConsultarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
