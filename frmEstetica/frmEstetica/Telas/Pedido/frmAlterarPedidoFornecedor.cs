﻿using frmEstetica.Fornecedor;
using frmEstetica.Pedido_Compra;
using frmEstetica.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica.Telas.Pedido
{
    public partial class frmAlterarPedidoFornecedor : Form
    {
        BindingList<FornecedorDTO> Fornecedor = new BindingList<FornecedorDTO>();
        BindingList<ProdutoDTO> Produto = new BindingList<ProdutoDTO>();
        public frmAlterarPedidoFornecedor()
        {
            InitializeComponent();
            CarregarCombo1();
            CarregarCombo2();
            CarregarCombo3();
        }
        void CarregarCombo1()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.empresa);
            cboFornecedor.DataSource = lista;
        }

        void CarregarCombo2()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista;
        }
        void CarregarCombo3()
        {
            PedidoCompraBusiness business = new PedidoCompraBusiness();
            List<PedidoCompraDTO> lista = business.Listar();

            cboId.ValueMember = nameof(PedidoCompraDTO.Id);
            cboId.DisplayMember = nameof(PedidoCompraDTO.Id);
            cboId.DataSource = lista;
        }


        private void frmAlterarPedidoFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO Fun = cboFornecedor.SelectedItem as FornecedorDTO;
                ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

                PedidoCompraDTO dto = new PedidoCompraDTO();
                dto.Id = Convert.ToInt32(cboId.Text);
                dto.FornecedorId = Fun.Id;
                dto.ProdutoId = Pro.IdProduto;
                dto.DtPedido = Convert.ToDateTime(txtData.Text);
                dto.QtdProduto = Convert.ToInt32(txtQuantidade.Text);

                if (dto.Id == 0 || dto.Id <= 0)
                {
                    MessageBox.Show("ID é obrigatório.");
                    return;
                }
                if (dto.QtdProduto == 0 || dto.QtdProduto <= 0)
                {
                    MessageBox.Show("Quantidade e necessario.");
                    return;
                }

               

                PedidoCompraBusiness business = new PedidoCompraBusiness();
                business.Alterar(dto);
                MessageBox.Show("Pedido alterado com sucesso.", "Wonderland", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Informações invalidas");
                return;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmPedirFornecedor ir = new frmPedirFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmConsultarPedidoFornecedor ir = new frmConsultarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void cboId_SelectedIndexChanged(object sender, EventArgs e)
        {
            PedidoCompraDTO Pro = cboId.SelectedItem as PedidoCompraDTO;

            PedidoCompraBusiness business = new PedidoCompraBusiness();
            PedidoCompraDTO prod = business.ListarPorId(Pro.Id);


            txtData.Text = prod.DtPedido.ToString();
            txtQuantidade.Text = prod.QtdProduto.ToString();
        }
    }
}
