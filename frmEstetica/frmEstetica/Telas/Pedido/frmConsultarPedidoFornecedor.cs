﻿using frmEstetica.Pedido_Compra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica.Telas.Pedido
{
    public partial class frmConsultarPedidoFornecedor : Form
    {
        public frmConsultarPedidoFornecedor()
        {
            InitializeComponent();
        }

        private void dgvPedidoFornecedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    PedidoCompraViewConsultar cat = dgvPedidoFornecedor.CurrentRow.DataBoundItem as PedidoCompraViewConsultar;

                    PedidoCompraBusiness business = new PedidoCompraBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse não pode ser removido agora");
                return;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PedidoCompraBusiness business = new PedidoCompraBusiness();
            List<PedidoCompraViewConsultar> lista = business.Consultar(txtEmpresa.Text);

            dgvPedidoFornecedor.AutoGenerateColumns = false;
            dgvPedidoFornecedor.DataSource = lista;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmPedirFornecedor ir = new frmPedirFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAlterarPedidoFornecedor ir = new frmAlterarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
