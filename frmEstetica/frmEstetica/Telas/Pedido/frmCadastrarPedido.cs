﻿using frmEstetica.Cliente;
using frmEstetica.Folha_de_Pagamento;
using frmEstetica.Funcionario;
using frmEstetica.Pedido_Venda;
using frmEstetica.Produto;
using frmEstetica.Telas.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{

    public partial class frmCadastrarPedido : Form
    {
        public frmCadastrarPedido()
        {
            InitializeComponent();
            CarregarCombo1();
            CarregarCombo2();
            CarregarCombo3();
        }
        void CarregarCombo1()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.Id);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.funcionario);
            cboFuncionario.DataSource = lista;
        }

        void CarregarCombo2()
        {
            ProdutoBusiness business2 = new ProdutoBusiness();
            List<ProdutoDTO> lista = business2.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista;
        }

        void CarregarCombo3()
        {
            ClienteBusiness business3 = new ClienteBusiness();
            List<ClienteDTO> lista = business3.Listar();

            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.nome);
            cboCliente.DataSource = lista;
        }
    

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void frmCadastrarPedido_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

            ProdutoBusiness business = new ProdutoBusiness();
            ProdutoDTO prod = business.ListarPorId(Pro.IdProduto);

            lblPreco.Text = prod.valorproduto.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;
                ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;
                ClienteDTO Cli = cboCliente.SelectedItem as ClienteDTO;

                PedidoVendaDTO dto = new PedidoVendaDTO();
                dto.ClienteId = Cli.Id;
                dto.FuncionarioId = Fun.Id;
                dto.ProdutoId = Pro.IdProduto;
                dto.quantidade = Convert.ToInt32(txtQuantidade.Text);
                dto.pagamento = txtPagamento.Text;
                dto.data = Convert.ToDateTime(txtData.Text);
                dto.valorfinal = Convert.ToDecimal(lblPreco.Text);
                dto.valortotal = Convert.ToDecimal(lblTotal.Text);

                lblTotal.Text = Convert.ToString((Convert.ToInt32(txtQuantidade.Text)) * (Convert.ToDecimal(lblPreco.Text)));
                dto.valortotal = Convert.ToDecimal(lblTotal.Text);

                if (dto.quantidade == 0 || dto.quantidade <= 0)
                {
                    MessageBox.Show("Quantidade e necessaria");
                    return;
                }

                if (dto.pagamento == string.Empty)
                {
                    MessageBox.Show("Selecione a forma de pagamento");
                    return;
                }

                PedidoVendaBusiness business = new PedidoVendaBusiness();
                business.Salvar(dto);
                MessageBox.Show("Pedido realizado com sucesso.", "Wonderland", MessageBoxButtons.OK, MessageBoxIcon.Information);

                frmCadastrarPedido ir = new frmCadastrarPedido();
                this.Hide();
                ir.ShowDialog();

            }
            catch (Exception)
            {

                MessageBox.Show("Informações Invalidas");
            }

            
        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void txtTotal_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void cboPreco_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void cboCliente_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAlterarPedido ir = new frmAlterarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmConsultarPedido ir = new frmConsultarPedido();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
