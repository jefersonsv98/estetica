﻿using frmEstetica.Pedido_Venda;
using frmEstetica.Telas.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmConsultarPedido : Form
    {
        public frmConsultarPedido()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PedidoVendaBusiness business = new PedidoVendaBusiness();
            List<PedidoVendaConsultarView> lista = business.Consultar(txtConsultarVenda.Text.Trim());

            dgvPedido.AutoGenerateColumns = false;
            dgvPedido.DataSource = lista;
        }

        private void dgvPedido_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 9)
                {
                    PedidoVendaConsultarView cat = dgvPedido.CurrentRow.DataBoundItem as PedidoVendaConsultarView;

                    PedidoVendaBusiness business = new PedidoVendaBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse é um dos pedidos que não pode ser removido");
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCadastrarPedido ir = new frmCadastrarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAlterarPedido ir = new frmAlterarPedido();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
