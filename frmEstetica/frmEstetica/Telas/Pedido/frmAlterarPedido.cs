﻿using frmEstetica.Cliente;
using frmEstetica.Funcionario;
using frmEstetica.Pedido_Venda;
using frmEstetica.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica.Telas.Pedido
{
    public partial class frmAlterarPedido : Form
    {
        public frmAlterarPedido()
        {
            InitializeComponent();
            CarregarCombo1();
            CarregarCombo2();
            CarregarCombo3();
            //CarregarCombo4();
        }

        void CarregarCombo1()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.Id);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.funcionario);
            cboFuncionario.DataSource = lista;
        }

        void CarregarCombo2()
        {
            ProdutoBusiness business2 = new ProdutoBusiness();
            List<ProdutoDTO> lista = business2.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista;
        }

        void CarregarCombo3()
        {
            ClienteBusiness business3 = new ClienteBusiness();
            List<ClienteDTO> lista = business3.Listar();

            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.nome);
            cboCliente.DataSource = lista;
        }
        //void CarregarCombo4()
        //{
        //    PedidoVendaBusiness business = new PedidoVendaBusiness();
        //    List<PedidoVendaDTO> lista = business.Listar();

        //    cboId.ValueMember = nameof(PedidoVendaDTO.Id);
        //    cboId.DisplayMember = nameof(PedidoVendaDTO.Id);
        //    cboId.DataSource = lista;
        //}

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;
                ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;
                ClienteDTO Cli = cboCliente.SelectedItem as ClienteDTO;

                PedidoVendaDTO dto = new PedidoVendaDTO();
                dto.Id = Convert.ToInt32(cboId.Text);
                dto.ClienteId = Cli.Id;
                dto.FuncionarioId = Fun.Id;
                dto.ProdutoId = Pro.IdProduto;
                dto.quantidade = Convert.ToInt32(txtQuantidade.Text);
                dto.pagamento = txtPagamento.Text;
                dto.data = txtData.Value;
                dto.valorfinal = Convert.ToDecimal(lblPreco.Text);
                dto.valortotal = Convert.ToDecimal(lblTotal.Text);

                lblTotal.Text = Convert.ToString((Convert.ToInt32(txtQuantidade.Text)) * (Convert.ToDecimal(lblPreco.Text)));
                dto.valortotal = Convert.ToDecimal(lblTotal.Text);


                if (dto.Id == 0)
                {
                    MessageBox.Show("ID é obrigatório.");
                    return;
                }

                if (dto.quantidade == 0)
                {
                    MessageBox.Show("Digite a quantidade");

                    return;
                }
                if (dto.pagamento == string.Empty)
                {
                    MessageBox.Show("Selecione a forma de pagamento");

                    return;
                }

                PedidoVendaBusiness business = new PedidoVendaBusiness();
                business.Alterar(dto);
                MessageBox.Show("Pedido alterado com sucesso.", "Wonderland", MessageBoxButtons.OK, MessageBoxIcon.Information);

                frmAlterarPedido ir = new frmAlterarPedido();
                this.Hide();
                ir.ShowDialog();

            }
            catch (Exception)
            {

                MessageBox.Show("Informaçoes Invalidas");
            }

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

            ProdutoBusiness business = new ProdutoBusiness();
            ProdutoDTO prod = business.ListarPorId(Pro.IdProduto);

            lblPreco.Text = prod.valorproduto.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtId_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCadastrarPedido ir = new frmCadastrarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmConsultarPedido ir = new frmConsultarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void cboId_SelectedIndexChanged(object sender, EventArgs e)
        {
            //PedidoVendaDTO Pro = cboId.SelectedItem as PedidoVendaDTO;

            //PedidoVendaBusiness business = new PedidoVendaBusiness();
            //PedidoVendaDTO prod = business.ListarPorId(Pro.Id);

            //txtQuantidade.Text = prod.quantidade.ToString();
            //txtPagamento.Text = prod.pagamento.ToString();
            //txtData.Text = prod.data.ToString();
            //lblPreco.Text = prod.valorfinal.ToString();
            //lblTotal.Text = prod.valortotal.ToString();
        }
    }
}
