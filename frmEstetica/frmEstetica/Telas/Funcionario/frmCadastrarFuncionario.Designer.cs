﻿namespace frmEstetica
{
    partial class frmCadastrarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbkfluxodecaixa = new System.Windows.Forms.CheckBox();
            this.cbkfolhadepagamento = new System.Windows.Forms.CheckBox();
            this.cbkfornecedor = new System.Windows.Forms.CheckBox();
            this.cbkestoque = new System.Windows.Forms.CheckBox();
            this.cbkcliente = new System.Windows.Forms.CheckBox();
            this.cbkfuncionario = new System.Windows.Forms.CheckBox();
            this.cbkproduto = new System.Windows.Forms.CheckBox();
            this.cbkpedido = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtcpf1 = new System.Windows.Forms.MaskedTextBox();
            this.txtrg1 = new System.Windows.Forms.MaskedTextBox();
            this.cboCargo = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cbosexo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtadmissao = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtnascimento = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.lblclose1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.txtCep = new System.Windows.Forms.MaskedTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.cbkAgendamento = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbkfluxodecaixa
            // 
            this.cbkfluxodecaixa.AutoSize = true;
            this.cbkfluxodecaixa.Location = new System.Drawing.Point(826, 437);
            this.cbkfluxodecaixa.Name = "cbkfluxodecaixa";
            this.cbkfluxodecaixa.Size = new System.Drawing.Size(95, 17);
            this.cbkfluxodecaixa.TabIndex = 267;
            this.cbkfluxodecaixa.Text = "Fluxo de Caixa";
            this.cbkfluxodecaixa.UseVisualStyleBackColor = true;
            this.cbkfluxodecaixa.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // cbkfolhadepagamento
            // 
            this.cbkfolhadepagamento.AutoSize = true;
            this.cbkfolhadepagamento.ForeColor = System.Drawing.Color.Crimson;
            this.cbkfolhadepagamento.Location = new System.Drawing.Point(581, 437);
            this.cbkfolhadepagamento.Name = "cbkfolhadepagamento";
            this.cbkfolhadepagamento.Size = new System.Drawing.Size(124, 17);
            this.cbkfolhadepagamento.TabIndex = 266;
            this.cbkfolhadepagamento.Text = "Folha de Pagamento";
            this.cbkfolhadepagamento.UseVisualStyleBackColor = true;
            this.cbkfolhadepagamento.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // cbkfornecedor
            // 
            this.cbkfornecedor.AutoSize = true;
            this.cbkfornecedor.Location = new System.Drawing.Point(826, 373);
            this.cbkfornecedor.Name = "cbkfornecedor";
            this.cbkfornecedor.Size = new System.Drawing.Size(80, 17);
            this.cbkfornecedor.TabIndex = 265;
            this.cbkfornecedor.Text = "Fornecedor";
            this.cbkfornecedor.UseVisualStyleBackColor = true;
            // 
            // cbkestoque
            // 
            this.cbkestoque.AutoSize = true;
            this.cbkestoque.ForeColor = System.Drawing.Color.Crimson;
            this.cbkestoque.Location = new System.Drawing.Point(726, 373);
            this.cbkestoque.Name = "cbkestoque";
            this.cbkestoque.Size = new System.Drawing.Size(65, 17);
            this.cbkestoque.TabIndex = 264;
            this.cbkestoque.Text = "Estoque";
            this.cbkestoque.UseVisualStyleBackColor = true;
            this.cbkestoque.CheckedChanged += new System.EventHandler(this.cbkestoque_CheckedChanged);
            // 
            // cbkcliente
            // 
            this.cbkcliente.AutoSize = true;
            this.cbkcliente.ForeColor = System.Drawing.Color.Crimson;
            this.cbkcliente.Location = new System.Drawing.Point(726, 437);
            this.cbkcliente.Name = "cbkcliente";
            this.cbkcliente.Size = new System.Drawing.Size(58, 17);
            this.cbkcliente.TabIndex = 263;
            this.cbkcliente.Text = "Cliente";
            this.cbkcliente.UseVisualStyleBackColor = true;
            this.cbkcliente.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // cbkfuncionario
            // 
            this.cbkfuncionario.AutoSize = true;
            this.cbkfuncionario.Location = new System.Drawing.Point(826, 406);
            this.cbkfuncionario.Name = "cbkfuncionario";
            this.cbkfuncionario.Size = new System.Drawing.Size(81, 17);
            this.cbkfuncionario.TabIndex = 260;
            this.cbkfuncionario.Text = "Funcionario";
            this.cbkfuncionario.UseVisualStyleBackColor = true;
            this.cbkfuncionario.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // cbkproduto
            // 
            this.cbkproduto.AutoSize = true;
            this.cbkproduto.ForeColor = System.Drawing.Color.Crimson;
            this.cbkproduto.Location = new System.Drawing.Point(726, 406);
            this.cbkproduto.Name = "cbkproduto";
            this.cbkproduto.Size = new System.Drawing.Size(63, 17);
            this.cbkproduto.TabIndex = 259;
            this.cbkproduto.Text = "Produto";
            this.cbkproduto.UseVisualStyleBackColor = true;
            this.cbkproduto.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // cbkpedido
            // 
            this.cbkpedido.AutoSize = true;
            this.cbkpedido.ForeColor = System.Drawing.Color.Crimson;
            this.cbkpedido.Location = new System.Drawing.Point(581, 406);
            this.cbkpedido.Name = "cbkpedido";
            this.cbkpedido.Size = new System.Drawing.Size(59, 17);
            this.cbkpedido.TabIndex = 258;
            this.cbkpedido.Text = "Pedido";
            this.cbkpedido.UseVisualStyleBackColor = true;
            this.cbkpedido.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Crimson;
            this.label9.Location = new System.Drawing.Point(686, 339);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 24);
            this.label9.TabIndex = 257;
            this.label9.Text = "Permissão:";
            // 
            // txtcelular
            // 
            this.txtcelular.BackColor = System.Drawing.Color.White;
            this.txtcelular.ForeColor = System.Drawing.Color.Black;
            this.txtcelular.Location = new System.Drawing.Point(806, 94);
            this.txtcelular.Mask = "(99) 00000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(89, 20);
            this.txtcelular.TabIndex = 10;
            this.txtcelular.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtcelular_MaskInputRejected);
            // 
            // txttelefone
            // 
            this.txttelefone.BackColor = System.Drawing.Color.White;
            this.txttelefone.ForeColor = System.Drawing.Color.Black;
            this.txttelefone.Location = new System.Drawing.Point(600, 97);
            this.txttelefone.Mask = "(99) 0000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(81, 20);
            this.txttelefone.TabIndex = 9;
            // 
            // txtcpf1
            // 
            this.txtcpf1.BackColor = System.Drawing.Color.White;
            this.txtcpf1.ForeColor = System.Drawing.Color.Black;
            this.txtcpf1.Location = new System.Drawing.Point(337, 246);
            this.txtcpf1.Mask = "999.999.999-99";
            this.txtcpf1.Name = "txtcpf1";
            this.txtcpf1.Size = new System.Drawing.Size(85, 20);
            this.txtcpf1.TabIndex = 4;
            // 
            // txtrg1
            // 
            this.txtrg1.BackColor = System.Drawing.Color.White;
            this.txtrg1.ForeColor = System.Drawing.Color.Black;
            this.txtrg1.Location = new System.Drawing.Point(120, 245);
            this.txtrg1.Mask = "99.999.999-9";
            this.txtrg1.Name = "txtrg1";
            this.txtrg1.Size = new System.Drawing.Size(73, 20);
            this.txtrg1.TabIndex = 3;
            this.txtrg1.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtrg1_MaskInputRejected);
            // 
            // cboCargo
            // 
            this.cboCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCargo.FormattingEnabled = true;
            this.cboCargo.Items.AddRange(new object[] {
            "Diretor Geral",
            "Administrador",
            "Recepcionista",
            "Esteticista",
            "Massagista",
            "Psicologa",
            "Acupuntura"});
            this.cboCargo.Location = new System.Drawing.Point(156, 291);
            this.cboCargo.Name = "cboCargo";
            this.cboCargo.Size = new System.Drawing.Size(266, 21);
            this.cboCargo.TabIndex = 5;
            this.cboCargo.SelectedIndexChanged += new System.EventHandler(this.txtcargo_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Crimson;
            this.label17.Location = new System.Drawing.Point(495, 154);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 24);
            this.label17.TabIndex = 250;
            this.label17.Text = "Cep:";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // cbosexo
            // 
            this.cbosexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosexo.FormattingEnabled = true;
            this.cbosexo.Items.AddRange(new object[] {
            "Maculino",
            "Feminino"});
            this.cbosexo.Location = new System.Drawing.Point(156, 383);
            this.cbosexo.Name = "cbosexo";
            this.cbosexo.Size = new System.Drawing.Size(266, 21);
            this.cbosexo.TabIndex = 7;
            this.cbosexo.SelectedIndexChanged += new System.EventHandler(this.txtsexo_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Crimson;
            this.label15.Location = new System.Drawing.Point(26, 380);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 24);
            this.label15.TabIndex = 248;
            this.label15.Text = "Sexo:";
            // 
            // txtadmissao
            // 
            this.txtadmissao.Location = new System.Drawing.Point(156, 439);
            this.txtadmissao.Name = "txtadmissao";
            this.txtadmissao.Size = new System.Drawing.Size(266, 20);
            this.txtadmissao.TabIndex = 8;
            this.txtadmissao.ValueChanged += new System.EventHandler(this.txtadm_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Crimson;
            this.label16.Location = new System.Drawing.Point(26, 437);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 24);
            this.label16.TabIndex = 246;
            this.label16.Text = "Admissão:";
            // 
            // txtNumero
            // 
            this.txtNumero.BackColor = System.Drawing.Color.White;
            this.txtNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.ForeColor = System.Drawing.Color.Black;
            this.txtNumero.Location = new System.Drawing.Point(600, 295);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(306, 22);
            this.txtNumero.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Crimson;
            this.label13.Location = new System.Drawing.Point(496, 295);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 24);
            this.label13.TabIndex = 244;
            this.label13.Text = "Nº";
            // 
            // txtBairro
            // 
            this.txtBairro.BackColor = System.Drawing.Color.White;
            this.txtBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBairro.ForeColor = System.Drawing.Color.Black;
            this.txtBairro.Location = new System.Drawing.Point(600, 248);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(306, 22);
            this.txtBairro.TabIndex = 14;
            this.txtBairro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBairro_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Crimson;
            this.label12.Location = new System.Drawing.Point(496, 248);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 24);
            this.label12.TabIndex = 242;
            this.label12.Text = "Bairro:";
            // 
            // txtRua
            // 
            this.txtRua.BackColor = System.Drawing.Color.White;
            this.txtRua.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRua.ForeColor = System.Drawing.Color.Black;
            this.txtRua.Location = new System.Drawing.Point(600, 203);
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(306, 22);
            this.txtRua.TabIndex = 13;
            this.txtRua.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRua_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(496, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 24);
            this.label1.TabIndex = 240;
            this.label1.Text = "Rua:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Crimson;
            this.label2.Location = new System.Drawing.Point(710, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 24);
            this.label2.TabIndex = 239;
            this.label2.Text = "Celular:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Crimson;
            this.label7.Location = new System.Drawing.Point(480, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 24);
            this.label7.TabIndex = 238;
            this.label7.Text = "Telefone:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Crimson;
            this.label6.Location = new System.Drawing.Point(26, 292);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 24);
            this.label6.TabIndex = 237;
            this.label6.Text = "Cargo:";
            // 
            // txtnascimento
            // 
            this.txtnascimento.Location = new System.Drawing.Point(156, 341);
            this.txtnascimento.Name = "txtnascimento";
            this.txtnascimento.Size = new System.Drawing.Size(266, 20);
            this.txtnascimento.TabIndex = 6;
            this.txtnascimento.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Crimson;
            this.label5.Location = new System.Drawing.Point(26, 340);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 24);
            this.label5.TabIndex = 235;
            this.label5.Text = "Nascimento:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Crimson;
            this.label4.Location = new System.Drawing.Point(26, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 24);
            this.label4.TabIndex = 234;
            this.label4.Text = "RG:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Crimson;
            this.label3.Location = new System.Drawing.Point(258, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 24);
            this.label3.TabIndex = 233;
            this.label3.Text = "CPF:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtnome
            // 
            this.txtnome.BackColor = System.Drawing.Color.White;
            this.txtnome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnome.ForeColor = System.Drawing.Color.Black;
            this.txtnome.Location = new System.Drawing.Point(116, 199);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(306, 22);
            this.txtnome.TabIndex = 2;
            this.txtnome.TextChanged += new System.EventHandler(this.txtnome_TextChanged);
            this.txtnome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnome_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Crimson;
            this.label8.Location = new System.Drawing.Point(26, 197);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 24);
            this.label8.TabIndex = 231;
            this.label8.Text = "Nome:";
            // 
            // txtemail
            // 
            this.txtemail.BackColor = System.Drawing.Color.White;
            this.txtemail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemail.ForeColor = System.Drawing.Color.Black;
            this.txtemail.Location = new System.Drawing.Point(116, 92);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(306, 22);
            this.txtemail.TabIndex = 0;
            this.txtemail.Text = "Crie um E-mail";
            this.txtemail.Click += new System.EventHandler(this.txtemail_Click);
            this.txtemail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtemail_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Crimson;
            this.label11.Location = new System.Drawing.Point(26, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 24);
            this.label11.TabIndex = 227;
            this.label11.Text = "Email:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Crimson;
            this.label14.Location = new System.Drawing.Point(26, 133);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 24);
            this.label14.TabIndex = 229;
            this.label14.Text = "Senha:";
            // 
            // txtsenha
            // 
            this.txtsenha.BackColor = System.Drawing.Color.White;
            this.txtsenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsenha.ForeColor = System.Drawing.Color.Black;
            this.txtsenha.Location = new System.Drawing.Point(116, 133);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(306, 24);
            this.txtsenha.TabIndex = 1;
            this.txtsenha.Text = "qualquer";
            this.txtsenha.UseSystemPasswordChar = true;
            this.txtsenha.Click += new System.EventHandler(this.txtsenha_Click);
            this.txtsenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsenha_KeyPress);
            // 
            // lblclose1
            // 
            this.lblclose1.AutoSize = true;
            this.lblclose1.BackColor = System.Drawing.Color.Crimson;
            this.lblclose1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblclose1.ForeColor = System.Drawing.Color.White;
            this.lblclose1.Location = new System.Drawing.Point(793, -54);
            this.lblclose1.Name = "lblclose1";
            this.lblclose1.Size = new System.Drawing.Size(23, 25);
            this.lblclose1.TabIndex = 226;
            this.lblclose1.Text = "x";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Crimson;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(770, -54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 25);
            this.label10.TabIndex = 225;
            this.label10.Text = "-";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Crimson;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(311, 488);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(314, 36);
            this.button4.TabIndex = 17;
            this.button4.Text = "Cadastrar Funcionario";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            this.button4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.button4_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.Crimson;
            this.label19.Location = new System.Drawing.Point(367, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(207, 39);
            this.label19.TabIndex = 268;
            this.label19.Text = "Funcionario";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Crimson;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(884, 12);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(27, 26);
            this.button5.TabIndex = 269;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtCep
            // 
            this.txtCep.BackColor = System.Drawing.Color.White;
            this.txtCep.ForeColor = System.Drawing.Color.Black;
            this.txtCep.Location = new System.Drawing.Point(600, 152);
            this.txtCep.Mask = "99999-999";
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(58, 20);
            this.txtCep.TabIndex = 11;
            this.txtCep.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtCep_MaskInputRejected);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Crimson;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(680, 152);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(71, 22);
            this.button2.TabIndex = 12;
            this.button2.Text = "Buscar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cbkAgendamento
            // 
            this.cbkAgendamento.AutoSize = true;
            this.cbkAgendamento.ForeColor = System.Drawing.Color.Crimson;
            this.cbkAgendamento.Location = new System.Drawing.Point(581, 373);
            this.cbkAgendamento.Name = "cbkAgendamento";
            this.cbkAgendamento.Size = new System.Drawing.Size(92, 17);
            this.cbkAgendamento.TabIndex = 274;
            this.cbkAgendamento.Text = "Agendamento";
            this.cbkAgendamento.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Crimson;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(12, 488);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(262, 36);
            this.button1.TabIndex = 16;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Crimson;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(657, 488);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(258, 36);
            this.button3.TabIndex = 18;
            this.button3.Text = "Consultar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // frmCadastrarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(927, 527);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbkAgendamento);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtCep);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.cbkfluxodecaixa);
            this.Controls.Add(this.cbkfolhadepagamento);
            this.Controls.Add(this.cbkfornecedor);
            this.Controls.Add(this.cbkestoque);
            this.Controls.Add(this.cbkcliente);
            this.Controls.Add(this.cbkfuncionario);
            this.Controls.Add(this.cbkproduto);
            this.Controls.Add(this.cbkpedido);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtcelular);
            this.Controls.Add(this.txttelefone);
            this.Controls.Add(this.txtcpf1);
            this.Controls.Add(this.txtrg1);
            this.Controls.Add(this.cboCargo);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.cbosexo);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtadmissao);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtBairro);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtRua);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtnascimento);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtsenha);
            this.Controls.Add(this.lblclose1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button4);
            this.ForeColor = System.Drawing.Color.Crimson;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastrarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmCadastrarFuncionario";
            this.Load += new System.EventHandler(this.frmCadastrarFuncionario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbkfluxodecaixa;
        private System.Windows.Forms.CheckBox cbkfolhadepagamento;
        private System.Windows.Forms.CheckBox cbkfornecedor;
        private System.Windows.Forms.CheckBox cbkestoque;
        private System.Windows.Forms.CheckBox cbkcliente;
        private System.Windows.Forms.CheckBox cbkfuncionario;
        private System.Windows.Forms.CheckBox cbkproduto;
        private System.Windows.Forms.CheckBox cbkpedido;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox txtcelular;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.MaskedTextBox txtcpf1;
        private System.Windows.Forms.MaskedTextBox txtrg1;
        private System.Windows.Forms.ComboBox cboCargo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbosexo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker txtadmissao;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker txtnascimento;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.Label lblclose1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.MaskedTextBox txtCep;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox cbkAgendamento;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
    }
}