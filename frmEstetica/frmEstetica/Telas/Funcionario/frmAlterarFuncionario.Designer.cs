﻿namespace frmEstetica
{
    partial class frmAlterarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.txtCep = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbkfluxodecaixa = new System.Windows.Forms.CheckBox();
            this.cbkfolhadepagamento = new System.Windows.Forms.CheckBox();
            this.cbkfornecedor = new System.Windows.Forms.CheckBox();
            this.cbkestoque = new System.Windows.Forms.CheckBox();
            this.cbkcliente = new System.Windows.Forms.CheckBox();
            this.cbkfuncionario = new System.Windows.Forms.CheckBox();
            this.cbkproduto = new System.Windows.Forms.CheckBox();
            this.cbkpedido = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtcpf1 = new System.Windows.Forms.MaskedTextBox();
            this.txtrg1 = new System.Windows.Forms.MaskedTextBox();
            this.cboCargo = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cbosexo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtadmissao = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtnascimento = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.cbkAgendamento = new System.Windows.Forms.CheckBox();
            this.txtId = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Crimson;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(672, 155);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(71, 22);
            this.button2.TabIndex = 13;
            this.button2.Text = "Buscar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtCep
            // 
            this.txtCep.BackColor = System.Drawing.Color.White;
            this.txtCep.ForeColor = System.Drawing.Color.Black;
            this.txtCep.Location = new System.Drawing.Point(589, 157);
            this.txtCep.Mask = "99999-999";
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(58, 20);
            this.txtCep.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.Color.Crimson;
            this.label19.Location = new System.Drawing.Point(284, 12);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(324, 39);
            this.label19.TabIndex = 313;
            this.label19.Text = "Alterar Funcionario";
            // 
            // cbkfluxodecaixa
            // 
            this.cbkfluxodecaixa.AutoSize = true;
            this.cbkfluxodecaixa.ForeColor = System.Drawing.Color.Crimson;
            this.cbkfluxodecaixa.Location = new System.Drawing.Point(813, 447);
            this.cbkfluxodecaixa.Name = "cbkfluxodecaixa";
            this.cbkfluxodecaixa.Size = new System.Drawing.Size(95, 17);
            this.cbkfluxodecaixa.TabIndex = 312;
            this.cbkfluxodecaixa.Text = "Fluxo de Caixa";
            this.cbkfluxodecaixa.UseVisualStyleBackColor = true;
            // 
            // cbkfolhadepagamento
            // 
            this.cbkfolhadepagamento.AutoSize = true;
            this.cbkfolhadepagamento.ForeColor = System.Drawing.Color.Crimson;
            this.cbkfolhadepagamento.Location = new System.Drawing.Point(589, 447);
            this.cbkfolhadepagamento.Name = "cbkfolhadepagamento";
            this.cbkfolhadepagamento.Size = new System.Drawing.Size(124, 17);
            this.cbkfolhadepagamento.TabIndex = 311;
            this.cbkfolhadepagamento.Text = "Folha de Pagamento";
            this.cbkfolhadepagamento.UseVisualStyleBackColor = true;
            // 
            // cbkfornecedor
            // 
            this.cbkfornecedor.AutoSize = true;
            this.cbkfornecedor.ForeColor = System.Drawing.Color.Crimson;
            this.cbkfornecedor.Location = new System.Drawing.Point(813, 382);
            this.cbkfornecedor.Name = "cbkfornecedor";
            this.cbkfornecedor.Size = new System.Drawing.Size(80, 17);
            this.cbkfornecedor.TabIndex = 310;
            this.cbkfornecedor.Text = "Fornecedor";
            this.cbkfornecedor.UseVisualStyleBackColor = true;
            // 
            // cbkestoque
            // 
            this.cbkestoque.AutoSize = true;
            this.cbkestoque.ForeColor = System.Drawing.Color.Crimson;
            this.cbkestoque.Location = new System.Drawing.Point(728, 382);
            this.cbkestoque.Name = "cbkestoque";
            this.cbkestoque.Size = new System.Drawing.Size(65, 17);
            this.cbkestoque.TabIndex = 309;
            this.cbkestoque.Text = "Estoque";
            this.cbkestoque.UseVisualStyleBackColor = true;
            // 
            // cbkcliente
            // 
            this.cbkcliente.AutoSize = true;
            this.cbkcliente.ForeColor = System.Drawing.Color.Crimson;
            this.cbkcliente.Location = new System.Drawing.Point(728, 447);
            this.cbkcliente.Name = "cbkcliente";
            this.cbkcliente.Size = new System.Drawing.Size(58, 17);
            this.cbkcliente.TabIndex = 308;
            this.cbkcliente.Text = "Cliente";
            this.cbkcliente.UseVisualStyleBackColor = true;
            // 
            // cbkfuncionario
            // 
            this.cbkfuncionario.AutoSize = true;
            this.cbkfuncionario.ForeColor = System.Drawing.Color.Crimson;
            this.cbkfuncionario.Location = new System.Drawing.Point(813, 413);
            this.cbkfuncionario.Name = "cbkfuncionario";
            this.cbkfuncionario.Size = new System.Drawing.Size(81, 17);
            this.cbkfuncionario.TabIndex = 307;
            this.cbkfuncionario.Text = "Funcionario";
            this.cbkfuncionario.UseVisualStyleBackColor = true;
            // 
            // cbkproduto
            // 
            this.cbkproduto.AutoSize = true;
            this.cbkproduto.ForeColor = System.Drawing.Color.Crimson;
            this.cbkproduto.Location = new System.Drawing.Point(728, 417);
            this.cbkproduto.Name = "cbkproduto";
            this.cbkproduto.Size = new System.Drawing.Size(63, 17);
            this.cbkproduto.TabIndex = 306;
            this.cbkproduto.Text = "Produto";
            this.cbkproduto.UseVisualStyleBackColor = true;
            // 
            // cbkpedido
            // 
            this.cbkpedido.AutoSize = true;
            this.cbkpedido.ForeColor = System.Drawing.Color.Crimson;
            this.cbkpedido.Location = new System.Drawing.Point(588, 417);
            this.cbkpedido.Name = "cbkpedido";
            this.cbkpedido.Size = new System.Drawing.Size(59, 17);
            this.cbkpedido.TabIndex = 305;
            this.cbkpedido.Text = "Pedido";
            this.cbkpedido.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Crimson;
            this.label9.Location = new System.Drawing.Point(678, 335);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 24);
            this.label9.TabIndex = 304;
            this.label9.Text = "Permissão:";
            // 
            // txtcelular
            // 
            this.txtcelular.BackColor = System.Drawing.Color.White;
            this.txtcelular.ForeColor = System.Drawing.Color.Black;
            this.txtcelular.Location = new System.Drawing.Point(804, 94);
            this.txtcelular.Mask = "(99) 00000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(89, 20);
            this.txtcelular.TabIndex = 11;
            // 
            // txttelefone
            // 
            this.txttelefone.BackColor = System.Drawing.Color.White;
            this.txttelefone.ForeColor = System.Drawing.Color.Black;
            this.txttelefone.Location = new System.Drawing.Point(588, 94);
            this.txttelefone.Mask = "(99) 0000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(81, 20);
            this.txttelefone.TabIndex = 10;
            // 
            // txtcpf1
            // 
            this.txtcpf1.BackColor = System.Drawing.Color.White;
            this.txtcpf1.ForeColor = System.Drawing.Color.Black;
            this.txtcpf1.Location = new System.Drawing.Point(327, 269);
            this.txtcpf1.Mask = "999.999.999-99";
            this.txtcpf1.Name = "txtcpf1";
            this.txtcpf1.Size = new System.Drawing.Size(84, 20);
            this.txtcpf1.TabIndex = 5;
            // 
            // txtrg1
            // 
            this.txtrg1.BackColor = System.Drawing.Color.White;
            this.txtrg1.ForeColor = System.Drawing.Color.Black;
            this.txtrg1.Location = new System.Drawing.Point(109, 273);
            this.txtrg1.Mask = "99.999.999-9";
            this.txtrg1.Name = "txtrg1";
            this.txtrg1.Size = new System.Drawing.Size(73, 20);
            this.txtrg1.TabIndex = 4;
            // 
            // cboCargo
            // 
            this.cboCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCargo.FormattingEnabled = true;
            this.cboCargo.Items.AddRange(new object[] {
            "Diretor Geral",
            "Administrador",
            "Recepcionista",
            "Esteticista",
            "Massagista",
            "Psicologa",
            "Acupuntura"});
            this.cboCargo.Location = new System.Drawing.Point(145, 319);
            this.cboCargo.Name = "cboCargo";
            this.cboCargo.Size = new System.Drawing.Size(266, 21);
            this.cboCargo.TabIndex = 6;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Crimson;
            this.label17.Location = new System.Drawing.Point(483, 152);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 24);
            this.label17.TabIndex = 298;
            this.label17.Text = "Cep:";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // cbosexo
            // 
            this.cbosexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosexo.FormattingEnabled = true;
            this.cbosexo.Items.AddRange(new object[] {
            "Maculino",
            "Feminino"});
            this.cbosexo.Location = new System.Drawing.Point(145, 411);
            this.cbosexo.Name = "cbosexo";
            this.cbosexo.Size = new System.Drawing.Size(266, 21);
            this.cbosexo.TabIndex = 8;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Crimson;
            this.label15.Location = new System.Drawing.Point(10, 411);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 24);
            this.label15.TabIndex = 296;
            this.label15.Text = "Sexo:";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // txtadmissao
            // 
            this.txtadmissao.Location = new System.Drawing.Point(145, 467);
            this.txtadmissao.Name = "txtadmissao";
            this.txtadmissao.Size = new System.Drawing.Size(266, 20);
            this.txtadmissao.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Crimson;
            this.label16.Location = new System.Drawing.Point(10, 465);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 24);
            this.label16.TabIndex = 294;
            this.label16.Text = "Admissão:";
            // 
            // txtNumero
            // 
            this.txtNumero.BackColor = System.Drawing.Color.White;
            this.txtNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.ForeColor = System.Drawing.Color.Black;
            this.txtNumero.Location = new System.Drawing.Point(588, 297);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(306, 22);
            this.txtNumero.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Crimson;
            this.label13.Location = new System.Drawing.Point(483, 294);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 24);
            this.label13.TabIndex = 292;
            this.label13.Text = "Nº";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // txtBairro
            // 
            this.txtBairro.BackColor = System.Drawing.Color.White;
            this.txtBairro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBairro.ForeColor = System.Drawing.Color.Black;
            this.txtBairro.Location = new System.Drawing.Point(588, 250);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(306, 22);
            this.txtBairro.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Crimson;
            this.label12.Location = new System.Drawing.Point(483, 250);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 24);
            this.label12.TabIndex = 290;
            this.label12.Text = "Bairro:";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // txtRua
            // 
            this.txtRua.BackColor = System.Drawing.Color.White;
            this.txtRua.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRua.ForeColor = System.Drawing.Color.Black;
            this.txtRua.Location = new System.Drawing.Point(588, 205);
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(306, 22);
            this.txtRua.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(483, 205);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 24);
            this.label1.TabIndex = 288;
            this.label1.Text = "Rua:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Crimson;
            this.label2.Location = new System.Drawing.Point(702, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 24);
            this.label2.TabIndex = 287;
            this.label2.Text = "Celular:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Crimson;
            this.label7.Location = new System.Drawing.Point(457, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 24);
            this.label7.TabIndex = 286;
            this.label7.Text = "Telefone:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Crimson;
            this.label6.Location = new System.Drawing.Point(15, 320);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 24);
            this.label6.TabIndex = 285;
            this.label6.Text = "Cargo:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // txtnascimento
            // 
            this.txtnascimento.Location = new System.Drawing.Point(145, 369);
            this.txtnascimento.Name = "txtnascimento";
            this.txtnascimento.Size = new System.Drawing.Size(266, 20);
            this.txtnascimento.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Crimson;
            this.label5.Location = new System.Drawing.Point(15, 368);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 24);
            this.label5.TabIndex = 283;
            this.label5.Text = "Nascimento:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Crimson;
            this.label4.Location = new System.Drawing.Point(15, 273);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 24);
            this.label4.TabIndex = 282;
            this.label4.Text = "RG:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Crimson;
            this.label3.Location = new System.Drawing.Point(247, 269);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 24);
            this.label3.TabIndex = 281;
            this.label3.Text = "CPF:";
            // 
            // txtnome
            // 
            this.txtnome.BackColor = System.Drawing.Color.White;
            this.txtnome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnome.ForeColor = System.Drawing.Color.Black;
            this.txtnome.Location = new System.Drawing.Point(105, 227);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(310, 22);
            this.txtnome.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Crimson;
            this.label8.Location = new System.Drawing.Point(15, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 24);
            this.label8.TabIndex = 279;
            this.label8.Text = "Nome:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // txtemail
            // 
            this.txtemail.BackColor = System.Drawing.Color.White;
            this.txtemail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemail.ForeColor = System.Drawing.Color.Black;
            this.txtemail.Location = new System.Drawing.Point(109, 138);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(306, 22);
            this.txtemail.TabIndex = 1;
            this.txtemail.Text = "Crie um E-mail";
            this.txtemail.Click += new System.EventHandler(this.txtemail_Click);
            this.txtemail.TextChanged += new System.EventHandler(this.txtemail_TextChanged);
            this.txtemail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtemail_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Crimson;
            this.label11.Location = new System.Drawing.Point(15, 136);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 24);
            this.label11.TabIndex = 275;
            this.label11.Text = "Email:";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Crimson;
            this.label14.Location = new System.Drawing.Point(10, 180);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 24);
            this.label14.TabIndex = 277;
            this.label14.Text = "Senha:";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // txtsenha
            // 
            this.txtsenha.BackColor = System.Drawing.Color.White;
            this.txtsenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsenha.ForeColor = System.Drawing.Color.Black;
            this.txtsenha.Location = new System.Drawing.Point(109, 181);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(306, 24);
            this.txtsenha.TabIndex = 2;
            this.txtsenha.Text = "qualquer";
            this.txtsenha.UseSystemPasswordChar = true;
            this.txtsenha.Click += new System.EventHandler(this.txtsenha_Click);
            this.txtsenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsenha_KeyPress);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Crimson;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(308, 508);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(300, 36);
            this.button4.TabIndex = 18;
            this.button4.Text = "Alterar Informação";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Crimson;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(868, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 26);
            this.button1.TabIndex = 317;
            this.button1.Text = "←";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Crimson;
            this.label10.Location = new System.Drawing.Point(29, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 24);
            this.label10.TabIndex = 320;
            this.label10.Text = "ID:";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // cbkAgendamento
            // 
            this.cbkAgendamento.AutoSize = true;
            this.cbkAgendamento.ForeColor = System.Drawing.Color.Crimson;
            this.cbkAgendamento.Location = new System.Drawing.Point(589, 382);
            this.cbkAgendamento.Name = "cbkAgendamento";
            this.cbkAgendamento.Size = new System.Drawing.Size(92, 17);
            this.cbkAgendamento.TabIndex = 322;
            this.cbkAgendamento.Text = "Agendamento";
            this.cbkAgendamento.UseVisualStyleBackColor = true;
            this.cbkAgendamento.CheckedChanged += new System.EventHandler(this.cbkAgendamento_CheckedChanged);
            // 
            // txtId
            // 
            this.txtId.FormattingEnabled = true;
            this.txtId.Location = new System.Drawing.Point(105, 88);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(69, 21);
            this.txtId.TabIndex = 334;
            this.txtId.SelectedIndexChanged += new System.EventHandler(this.txtId_SelectedIndexChanged);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Crimson;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(14, 508);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(268, 36);
            this.button3.TabIndex = 17;
            this.button3.Text = "Cadastrar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Crimson;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(635, 508);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(273, 36);
            this.button5.TabIndex = 19;
            this.button5.Text = "Consultar";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // frmAlterarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(914, 551);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.cbkAgendamento);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtCep);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.cbkfluxodecaixa);
            this.Controls.Add(this.cbkfolhadepagamento);
            this.Controls.Add(this.cbkfornecedor);
            this.Controls.Add(this.cbkestoque);
            this.Controls.Add(this.cbkcliente);
            this.Controls.Add(this.cbkfuncionario);
            this.Controls.Add(this.cbkproduto);
            this.Controls.Add(this.cbkpedido);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtcelular);
            this.Controls.Add(this.txttelefone);
            this.Controls.Add(this.txtcpf1);
            this.Controls.Add(this.txtrg1);
            this.Controls.Add(this.cboCargo);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.cbosexo);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtadmissao);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtBairro);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtRua);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtnascimento);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtsenha);
            this.Controls.Add(this.button4);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAlterarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmAlterarFuncionario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.MaskedTextBox txtCep;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox cbkfluxodecaixa;
        private System.Windows.Forms.CheckBox cbkfolhadepagamento;
        private System.Windows.Forms.CheckBox cbkfornecedor;
        private System.Windows.Forms.CheckBox cbkestoque;
        private System.Windows.Forms.CheckBox cbkcliente;
        private System.Windows.Forms.CheckBox cbkfuncionario;
        private System.Windows.Forms.CheckBox cbkproduto;
        private System.Windows.Forms.CheckBox cbkpedido;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox txtcelular;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.MaskedTextBox txtcpf1;
        private System.Windows.Forms.MaskedTextBox txtrg1;
        private System.Windows.Forms.ComboBox cboCargo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbosexo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker txtadmissao;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker txtnascimento;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox cbkAgendamento;
        private System.Windows.Forms.ComboBox txtId;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
    }
}