﻿namespace frmEstetica
{
    partial class frmConsultarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvFuncionario = new System.Windows.Forms.DataGridView();
            this.ID_Fornecedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_empresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_rg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_cpf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_cargo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_nascimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_sexo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_celular = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_rua = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_bairro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_cep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_admissao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtConsultarFuncionario = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionario)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFuncionario
            // 
            this.dgvFuncionario.AccessibleRole = System.Windows.Forms.AccessibleRole.IpAddress;
            this.dgvFuncionario.AllowUserToAddRows = false;
            this.dgvFuncionario.AllowUserToDeleteRows = false;
            this.dgvFuncionario.BackgroundColor = System.Drawing.Color.Crimson;
            this.dgvFuncionario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFuncionario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Fornecedor,
            this.nm_empresa,
            this.nm_funcionario,
            this.nu_rg,
            this.nu_cpf,
            this.ds_cargo,
            this.dt_nascimento,
            this.ds_sexo,
            this.nu_telefone,
            this.nu_celular,
            this.nm_rua,
            this.nm_bairro,
            this.nu_numero,
            this.nu_cep,
            this.dt_admissao,
            this.Column3});
            this.dgvFuncionario.Location = new System.Drawing.Point(12, 122);
            this.dgvFuncionario.Name = "dgvFuncionario";
            this.dgvFuncionario.ReadOnly = true;
            this.dgvFuncionario.RowHeadersVisible = false;
            this.dgvFuncionario.Size = new System.Drawing.Size(575, 345);
            this.dgvFuncionario.TabIndex = 157;
            this.dgvFuncionario.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFuncionario_CellClick_1);
            // 
            // ID_Fornecedor
            // 
            this.ID_Fornecedor.DataPropertyName = "ID";
            this.ID_Fornecedor.HeaderText = "ID";
            this.ID_Fornecedor.Name = "ID_Fornecedor";
            this.ID_Fornecedor.ReadOnly = true;
            // 
            // nm_empresa
            // 
            this.nm_empresa.DataPropertyName = "email";
            this.nm_empresa.HeaderText = "Email";
            this.nm_empresa.Name = "nm_empresa";
            this.nm_empresa.ReadOnly = true;
            // 
            // nm_funcionario
            // 
            this.nm_funcionario.DataPropertyName = "Funcionario";
            this.nm_funcionario.HeaderText = "Funcionario";
            this.nm_funcionario.Name = "nm_funcionario";
            this.nm_funcionario.ReadOnly = true;
            // 
            // nu_rg
            // 
            this.nu_rg.DataPropertyName = "RG";
            this.nu_rg.HeaderText = "RG";
            this.nu_rg.Name = "nu_rg";
            this.nu_rg.ReadOnly = true;
            // 
            // nu_cpf
            // 
            this.nu_cpf.DataPropertyName = "CPF";
            this.nu_cpf.HeaderText = "CPF";
            this.nu_cpf.Name = "nu_cpf";
            this.nu_cpf.ReadOnly = true;
            // 
            // ds_cargo
            // 
            this.ds_cargo.DataPropertyName = "Cargo";
            this.ds_cargo.HeaderText = "Cargo";
            this.ds_cargo.Name = "ds_cargo";
            this.ds_cargo.ReadOnly = true;
            // 
            // dt_nascimento
            // 
            this.dt_nascimento.DataPropertyName = "Nascimento";
            this.dt_nascimento.HeaderText = "Nascimento";
            this.dt_nascimento.Name = "dt_nascimento";
            this.dt_nascimento.ReadOnly = true;
            // 
            // ds_sexo
            // 
            this.ds_sexo.DataPropertyName = "Sexo";
            this.ds_sexo.HeaderText = "Sexo";
            this.ds_sexo.Name = "ds_sexo";
            this.ds_sexo.ReadOnly = true;
            // 
            // nu_telefone
            // 
            this.nu_telefone.DataPropertyName = "tel";
            this.nu_telefone.HeaderText = "Telefone";
            this.nu_telefone.Name = "nu_telefone";
            this.nu_telefone.ReadOnly = true;
            // 
            // nu_celular
            // 
            this.nu_celular.DataPropertyName = "celular";
            this.nu_celular.HeaderText = "Celular";
            this.nu_celular.Name = "nu_celular";
            this.nu_celular.ReadOnly = true;
            // 
            // nm_rua
            // 
            this.nm_rua.DataPropertyName = "rua";
            this.nm_rua.HeaderText = "Rua";
            this.nm_rua.Name = "nm_rua";
            this.nm_rua.ReadOnly = true;
            // 
            // nm_bairro
            // 
            this.nm_bairro.DataPropertyName = "bairro";
            this.nm_bairro.HeaderText = "Bairro";
            this.nm_bairro.Name = "nm_bairro";
            this.nm_bairro.ReadOnly = true;
            // 
            // nu_numero
            // 
            this.nu_numero.DataPropertyName = "numero";
            this.nu_numero.HeaderText = "Nº";
            this.nu_numero.Name = "nu_numero";
            this.nu_numero.ReadOnly = true;
            // 
            // nu_cep
            // 
            this.nu_cep.DataPropertyName = "cep";
            this.nu_cep.HeaderText = "CEP";
            this.nu_cep.Name = "nu_cep";
            this.nu_cep.ReadOnly = true;
            // 
            // dt_admissao
            // 
            this.dt_admissao.DataPropertyName = "admissao";
            this.dt_admissao.HeaderText = "Admissao";
            this.dt_admissao.Name = "dt_admissao";
            this.dt_admissao.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "";
            this.Column3.Image = global::frmEstetica.Properties.Resources.download;
            this.Column3.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column3.Width = 25;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Crimson;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(561, 5);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(31, 26);
            this.button5.TabIndex = 4;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Crimson;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(12, 90);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(72, 26);
            this.button4.TabIndex = 1;
            this.button4.Text = "Consultar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtConsultarFuncionario
            // 
            this.txtConsultarFuncionario.Location = new System.Drawing.Point(214, 57);
            this.txtConsultarFuncionario.Name = "txtConsultarFuncionario";
            this.txtConsultarFuncionario.Size = new System.Drawing.Size(268, 20);
            this.txtConsultarFuncionario.TabIndex = 0;
            this.txtConsultarFuncionario.TextChanged += new System.EventHandler(this.txtConsultar_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(13, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 24);
            this.label1.TabIndex = 151;
            this.label1.Text = "Nome do Funcionario";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Crimson;
            this.label15.Location = new System.Drawing.Point(150, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(280, 31);
            this.label15.TabIndex = 150;
            this.label15.Text = "Consultar Funcionario";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "Column1";
            this.dataGridViewImageColumn1.Image = global::frmEstetica.Properties.Resources.download;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 25;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Crimson;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(108, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 26);
            this.button1.TabIndex = 2;
            this.button1.Text = "Cadastrar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Crimson;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(205, 90);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(72, 26);
            this.button2.TabIndex = 3;
            this.button2.Text = "Alterar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmConsultarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(599, 479);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgvFuncionario);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtConsultarFuncionario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label15);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmConsultarFuncionario";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFuncionario;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtConsultarFuncionario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Fornecedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_empresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_rg;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_cpf;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_cargo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_nascimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_sexo;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_celular;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_rua;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_bairro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_cep;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_admissao;
        private System.Windows.Forms.DataGridViewImageColumn Column3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}