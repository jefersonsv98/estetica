﻿using frmEstetica.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmConsultarFuncionario : Form
    {
        public frmConsultarFuncionario()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Consultar(txtConsultarFuncionario.Text.Trim());

            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }
            private void txtConsultar_TextChanged(object sender, EventArgs e)
            {
 
            }

        private void dgvFuncionario_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvFuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvFuncionario_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 15)
                {
                    FuncionarioDTO cat = dgvFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;

                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse é um dos produtos que não pode ser removido");
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario ir = new frmCadastrarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmAlterarFuncionario ir = new frmAlterarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
