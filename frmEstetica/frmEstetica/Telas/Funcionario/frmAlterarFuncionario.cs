﻿using frmEstetica.API.Correio;
using frmEstetica.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmAlterarFuncionario : Form
    {
        public frmAlterarFuncionario()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            txtId.ValueMember = nameof(FuncionarioDTO.Id);
            txtId.DisplayMember = nameof(FuncionarioDTO.Id);
            txtId.DataSource = lista;
        }


        private void button4_Click(object sender, EventArgs e)
        {

            try
            {

                FuncionarioDTO dto = new FuncionarioDTO();
                

                dto.Id = Convert.ToInt32(txtId.Text.Trim());
                dto.email = txtemail.Text.Trim();
                dto.senha = txtsenha.Text.Trim();
                dto.funcionario = txtnome.Text.Trim();
                dto.rg = txtrg1.Text.Trim();
                dto.cpf = txtcpf1.Text.Trim();
                dto.cargo = cboCargo.Text;
                dto.nascimento = Convert.ToDateTime(txtnascimento.Text);
                dto.tel = txttelefone.Text.Trim();
                dto.celular = txtcelular.Text.Trim();
                dto.rua = txtRua.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.numero = txtNumero.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.sexo = cbosexo.Text;
                dto.admissao = Convert.ToDateTime(txtadmissao.Text);

                dto.PermissaoProduto = cbkproduto.Checked;
                dto.PermissaoCliente = cbkcliente.Checked;
                dto.PermissaoEstoque = cbkestoque.Checked;
                dto.PermissaoPedido = cbkpedido.Checked;
                dto.PermissaoFolhaPagamento = cbkfolhadepagamento.Checked;
                dto.PermissaoFornecedor = cbkfornecedor.Checked;
                dto.PermissaoFuncionario = cbkfuncionario.Checked;
                dto.PermissaoFluxoCaixa = cbkfluxodecaixa.Checked;
                dto.PermissaoAgendamento = cbkAgendamento.Checked;



                if (dto.email == string.Empty || dto.email == "Digite seu email")
                {
                    MessageBox.Show("Digite seu email");

                    return;
                }
                if (dto.senha == string.Empty || dto.senha == "Digite sua senha")
                {
                    MessageBox.Show("Digite sua senha");

                    return;
                }
                if (dto.funcionario == string.Empty || dto.funcionario == "Digite seu nome")
                {
                    MessageBox.Show("Digite o Nome");

                    return;
                }
                if (dto.rg == string.Empty || dto.rg == "Digite seu rg" || txtrg1.Text.Length <= 9)
                {
                    MessageBox.Show("Digite o rg");

                    return;
                }
                if (dto.cpf == string.Empty || dto.cpf == "Digite seu cpf" || txtcpf1.Text.Length <= 11)
                {
                    MessageBox.Show("Digite seu cpf");

                    return;
                }
                if (dto.cargo == string.Empty || dto.cargo == "Selecione o cargo")
                {
                    MessageBox.Show("Selecione seu Cargo");

                    return;
                }
                if (dto.tel == string.Empty || dto.tel == "Digite seu nome" || txttelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Digite seu telefone");

                    return;
                }
                if (dto.celular == string.Empty || dto.celular == "Digite seu celular" || txtcelular.Text.Length <= 14)
                {
                    MessageBox.Show("Digite seu celular");

                    return;
                }
                if (dto.rua == string.Empty || dto.rua == "Digite sua rua")
                {
                    MessageBox.Show("Digite sua rua");

                    return;
                }
                if (dto.bairro == string.Empty || dto.bairro == "Digite seu bairro")
                {
                    MessageBox.Show("Digite o bairro");

                    return;
                }
                if (dto.numero == string.Empty || dto.numero == "Nº")
                {
                    MessageBox.Show("Digite o numero da casa");

                    return;
                }
                if (dto.cep == string.Empty || txtCep.Text.Length <= 8 || dto.cep == "Digite o cep")
                {
                    MessageBox.Show("Digite seu cep corretamente");

                    return;
                }
                

                FuncionarioDataBase db = new FuncionarioDataBase();
                db.Alterar(dto);

                MessageBox.Show("Informação alterada com sucesso.", "Wonderland",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

        }
            catch (Exception)
            {
                MessageBox.Show("Tente novamente!");
                return;
                
            }



}

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;

            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void txtemail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtemail_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmAlterarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        { 

            frmMenu ir = new frmMenu();
                this.Hide();
            ir.ShowDialog();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label20_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void Procurar_Click(object sender, EventArgs e)
        {
            //FuncionarioBusiness business = new FuncionarioBusiness();
            //FuncionarioDTO listar = business.Consultar(txtnome.Text.Trim());
            //txtId.Text = listar.Id.ToString();
            //txtemail.Text = listar.email;
            //txtnascimento.Value = listar.nascimento;
            //txtrg1.Text = listar.rg;
            //txtcpf1.Text = listar.cpf;
            //txtadmissao.Value = listar.admissao;
            //txttelefone.Text = listar.telefone;
            //txtcelular.Text = listar.celular;
            //txtNumero.Text = listar.numero;
            
            
        }

        private void cbkAgendamento_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtemail_Click(object sender, EventArgs e)
        {
            if (txtemail.Text == "Crie um E-mail")
            {
                txtemail.Text = string.Empty;
            }
        }

        private void txtsenha_Click(object sender, EventArgs e)
        {
            if (txtsenha.Text == "qualquer")
            {
                txtsenha.Text = string.Empty;
            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtId_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Pro = txtId.SelectedItem as FuncionarioDTO;

                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO prod = business.ListarPorId(Pro.Id);

                txtemail.Text = prod.email.ToString();
                txtnome.Text = prod.funcionario.ToString();
                cboCargo.Text = prod.cargo.ToString();
                txtnascimento.Text = prod.nascimento.ToString();
                cbosexo.Text = prod.sexo.ToString();
                txtadmissao.Text = prod.admissao.ToString();
                txttelefone.Text = prod.tel.ToString();
                txtcelular.Text = prod.celular.ToString();
                txtCep.Text = prod.cep.ToString();
                txtNumero.Text = prod.numero.ToString();
                txtrg1.Text = prod.rg.ToString();
                txtcpf1.Text = prod.cpf.ToString();
                cbkproduto.Checked = prod.PermissaoProduto;
                cbkcliente.Checked = prod.PermissaoCliente;
                cbkestoque.Checked = prod.PermissaoEstoque;
                cbkpedido.Checked = prod.PermissaoPedido;
                cbkfolhadepagamento.Checked = prod.PermissaoFolhaPagamento;
                cbkfornecedor.Checked = prod.PermissaoFornecedor;
                cbkfuncionario.Checked = prod.PermissaoFuncionario;
                cbkfluxodecaixa.Checked = prod.PermissaoFluxoCaixa;
                cbkAgendamento.Checked = prod.PermissaoAgendamento;
                return;
            }
            catch (Exception)
            {
                return;
            }
            

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario ir = new frmCadastrarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario ir = new frmConsultarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
