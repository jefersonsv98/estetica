﻿using frmEstetica.API.Correio;
using frmEstetica.Funcionario;
using frmEstetica.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmCadastrarFuncionario : Form
    {
        public frmCadastrarFuncionario()
        {
            InitializeComponent();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtadm_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();


                dto.email = txtemail.Text.Trim();
                dto.senha = txtsenha.Text.Trim();
                dto.funcionario = txtnome.Text.Trim();
                dto.rg = txtrg1.Text.Trim();
                dto.cpf = txtcpf1.Text.Trim();
                dto.cargo = cboCargo.Text;
                dto.nascimento = Convert.ToDateTime(txtnascimento.Text);
                dto.tel = txttelefone.Text.Trim();
                dto.celular = txtcelular.Text.Trim();
                dto.rua = txtRua.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.numero = txtNumero.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.sexo = cbosexo.Text.Trim();
                dto.admissao = Convert.ToDateTime(txtadmissao.Text);

                dto.PermissaoProduto = cbkproduto.Checked;
                dto.PermissaoCliente = cbkcliente.Checked;
                dto.PermissaoEstoque = cbkestoque.Checked;
                dto.PermissaoPedido = cbkpedido.Checked;
                dto.PermissaoFolhaPagamento = cbkfolhadepagamento.Checked;
                dto.PermissaoFornecedor = cbkfornecedor.Checked;
                dto.PermissaoFuncionario = cbkfuncionario.Checked;
                dto.PermissaoFluxoCaixa = cbkfluxodecaixa.Checked;
                dto.PermissaoAgendamento = cbkAgendamento.Checked;

                if (dto.email == string.Empty || dto.email == "Crie um E-mail")
                {
                    MessageBox.Show("Crie um e-mail para login.");
                    return;
                }

                if (dto.senha == string.Empty)
                {
                    MessageBox.Show("Crie uma senha para login.");
                    return;
                }

                if (dto.funcionario == string.Empty || dto.funcionario == "Nome Completo")
                {
                    MessageBox.Show("Coloque o nome do funcionario.");
                    return;
                }

                if (dto.rg == string.Empty || txtrg1.Text.Length <= 11)
                {
                    MessageBox.Show("Digite os numeros da RG.");
                    return;
                }

                if (dto.cpf == string.Empty || txtcpf1.Text.Length <= 11)
                {
                    MessageBox.Show("Digite os numeros do CPF.");
                    return;
                }

                if (dto.cargo == string.Empty)
                {
                    MessageBox.Show("Selecione o cargo.");
                    return;
                }

                if (dto.tel == string.Empty || txttelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Telefone é obrigatório.");
                    return;
                }

                if (dto.celular == string.Empty || txtcelular.Text.Length <= 14)
                {
                    MessageBox.Show("Celular é obrigatório.");
                    return;
                }

                if (dto.rua == string.Empty || dto.rua == "Nome da rua")
                {
                    MessageBox.Show("Rua é obrigatória.");
                    return;
                }

                if (dto.bairro == string.Empty || dto.bairro == "Bairro que mora")
                {
                    MessageBox.Show("Bairro é obrigatório.");
                    return;
                }

                if (dto.numero == string.Empty || dto.numero == "Nº da casa")
                {
                    MessageBox.Show("Numero é obrigatório.");
                    return;
                }

                if (dto.cep == string.Empty || txtCep.Text.Length <= 8)
                {
                    MessageBox.Show("Digite os numeros do CEP.");
                    return;
                }

                if (dto.sexo == string.Empty)
                {
                    MessageBox.Show("Selecione o sexo.");
                    return;

                }
                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);

                MessageBox.Show("Funcionario salvo.", "Wonderland",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                frmCadastrarFuncionario ir = new frmCadastrarFuncionario();
                this.Hide();
                ir.ShowDialog();
            }
            catch (Exception)
            {

                MessageBox.Show("Dados Invalidos");
            }

            
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtrg1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtcargo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtsexo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmCadastrarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                frmMenu retroceder = new frmMenu();
                this.Hide();
                retroceder.ShowDialog();
            }
            catch (Exception)
            {

               // MessageBox.Show("Crie uma conta para acessar o programa");
                frmLogar retroceder = new frmLogar();
                this.Hide();
                retroceder.ShowDialog();
            }
            
        }

        private void label20_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void button4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;
              
            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void cbkestoque_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtemail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtCep_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void txtsenha_Click(object sender, EventArgs e)
        {
            if (txtsenha.Text == "qualquer")
            {
                txtsenha.Text = string.Empty;
            }
        }

        private void txtemail_Click(object sender, EventArgs e)
        {
            if (txtemail.Text == "Crie um E-mail")
            {
                txtemail.Text = string.Empty;
            }
        }

        private void txtcelular_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAlterarFuncionario ir = new frmAlterarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario ir = new frmConsultarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
