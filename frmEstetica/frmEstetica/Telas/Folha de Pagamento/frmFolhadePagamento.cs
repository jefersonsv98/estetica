﻿using frmEstetica.Folha_de_Pagamento;
using frmEstetica.Funcionario;
using frmEstetica.Telas.Folha_de_Pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmFolhadePagamento : Form
    {
        public frmFolhadePagamento()
        {
            InitializeComponent();
            CarregarCombo();
        }

        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.Id);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.funcionario);
            cboFuncionario.DataSource = lista;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu retroceder = new frmMenu();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;

                PagamentoDTO dto = new PagamentoDTO();
                dto.FuncionarioId = Fun.Id;
                dto.data = Convert.ToDateTime(txtData.Text);
                dto.horames = txtHoraMes.Text;
                dto.salariohora = txtSalarioHora.Text;
                dto.valor = txtValor.Text;
                dto.aliquota = txtAliquota.Text;
                dto.inss = txtInss.Text;
                dto.salariofinal = txtSalarioFinal.Text;
                dto.horaext50 = txtHr1.Text;
                dto.horaext100 = txtHr2.Text;
                dto.vlrefeicao = txtRefeicao.Text;
                dto.vltransporte = txtTransporte.Text;

                if (txtHoraMes.Text == string.Empty)
                {
                    MessageBox.Show("É preciso informar a quantidades de horas trabalhadas!");
                    return;
                }

                if (txtSalarioHora.Text == string.Empty)
                {
                    MessageBox.Show("É preciso informar o valor do salário hora!");
                    return;
                }

                if (txtAliquota.Text == string.Empty)
                {
                    MessageBox.Show("É preciso informar a aliquota para calculo do inss!");
                    return;
                }

                txtValor.Text = Convert.ToString((Convert.ToDecimal(txtHoraMes.Text)) * (Convert.ToDecimal(txtSalarioHora.Text)));
                dto.valor = txtValor.Text;

                txtInss.Text = Convert.ToString((Convert.ToDecimal(txtValor.Text)) * ((Convert.ToDecimal(txtAliquota.Text)) / 100));
                dto.inss = txtInss.Text;

                txtSalarioFinal.Text = Convert.ToString((Convert.ToDecimal(txtValor.Text)) - (Convert.ToDecimal(txtInss.Text)));
                dto.salariofinal = txtSalarioFinal.Text;


                PagamentoBusiness business = new PagamentoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Folha de pagamento salvo com sucesso.", "Wonderland", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Informações invalidas.", "Wonderland", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

            private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
            {
              FuncionarioDTO dto = cboFuncionario.SelectedItem as FuncionarioDTO;
            }

        private void txtHoraMes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtSalarioHora_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtValor_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAliquota_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtHr1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtHr2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtRefeicao_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRefeicao_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtTransporte_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void frmFolhadePagamento_Load(object sender, EventArgs e)
        {

        }

        private void txtSalarioFinal_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmAlterarPagamento ir = new frmAlterarPagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmConsultarFolhadePagamento ir = new frmConsultarFolhadePagamento();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
