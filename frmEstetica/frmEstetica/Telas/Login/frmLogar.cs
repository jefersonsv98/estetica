﻿using frmEstetica.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmLogar : Form
    {
        public frmLogar()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO funcionario = business.Logar(txtemail.Text, txtsenha.Text);

                if (funcionario != null)
                {
                    UserSession.UsuarioLogado = funcionario;

                    frmMenu menu = new frmMenu();
                    menu.Show();
                    this.Hide();
                }

                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Wonderland", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }

            catch (ArgumentException)
            {
                MessageBox.Show("Preencha os dois campos para logar.", "Wonderland",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblclose1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario cadastrar = new frmCadastrarFuncionario();
            this.Hide();
            cadastrar.Show();
        }

        private void txtemail_Click(object sender, EventArgs e)
        {
            if (txtemail.Text == "Digite seu email")
            {
                txtemail.Text = string.Empty;
            }
        }

        private void txtsenha_Click(object sender, EventArgs e)
        {
            if (txtsenha.Text == "00000000")
            {
                txtsenha.Text = string.Empty;
            }
        }
    }
}
