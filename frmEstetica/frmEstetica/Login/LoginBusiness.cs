﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica.Login
{
    class loginBusiness
        {
            LoginDatabase db = new LoginDatabase();
            public bool Logar(string nome, string senha)
            {
                bool logou = db.Logar(nome, senha);
                return logou;
            }

            public int Salvar(loginDTO dados)
            {

                if (dados.nome == string.Empty)
                {
                    frmLogar aqui = new frmLogar();
                    aqui.ShowDialog();

                    MessageBox.Show("E-mail é obrigatório", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }


                if (dados.senha == string.Empty)
                {
                    frmCadastrarFuncionario aqui = new frmCadastrarFuncionario();
                    aqui.ShowDialog();

                    MessageBox.Show("Senha é obrigatório", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                LoginDatabase pedido = new LoginDatabase();
                int Id = pedido.Salvar(dados);
                return Id;
            }

    }
}

