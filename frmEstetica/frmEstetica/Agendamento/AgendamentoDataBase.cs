﻿using frmEstetica.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Agendamento
{
    class AgendamentoDataBase
    {
        public int Salvar(AgendamentoDTO dto)
        {
            string script = @"INSERT INTO TB_Agendamento (nm_nome,ds_cpf,nr_telefone,ds_rua,ds_bairro,ds_numero_casa,ds_cep,dt_hora,dt_data,ds_observacao,tb_cliente_ID_Cliente,tb_funcionario_id_funcionario)
                                              VALUES (@nm_nome,@ds_cpf,@nr_telefone,@ds_rua,@ds_bairro,@ds_numero_casa,@ds_cep,@dt_hora,@dt_data,@ds_observacao,@tb_cliente_ID_Cliente,@tb_funcionario_id_funcionario)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.cpf));
            parms.Add(new MySqlParameter("nr_telefone", dto.telefone));
            parms.Add(new MySqlParameter("ds_rua", dto.rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.bairro));
            parms.Add(new MySqlParameter("ds_numero_casa", dto.numero));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("dt_hora", dto.hora));
            parms.Add(new MySqlParameter("dt_data", dto.data));
            parms.Add(new MySqlParameter("ds_observacao", dto.observacao));
            parms.Add(new MySqlParameter("tb_cliente_ID_Cliente", dto.IDcliente));
            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dto.Idfuncionarios));





            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(AgendamentoDTO dto)
        {
            string script = @"UPDATE TB_Agendamento
                                 SET nm_nome = @nm_nome,
                                     ds_cpf = @ds_cpf,
                                     nr_telefone = @nr_telefone,
                                     ds_rua = @ds_rua,
                                     ds_bairro = @ds_bairro,
                                     ds_numero_casa = @ds_numero_casa,
                                     ds_cep = @ds_cep,
                                     dt_hora = @dt_hora,
                                     dt_data = @dt_data,
                                     ds_observacao = @ds_observacao,
                                     tb_cliente_ID_Cliente = @tb_cliente_ID_Cliente,
                                     tb_funcionario_id_funcionario = @tb_funcionario_id_funcionario
                                    WHERE ID_Agendamento = @ID_Agendamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Agendamento", dto.Id));
            parms.Add(new MySqlParameter("nm_nome", dto.nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.cpf));
            parms.Add(new MySqlParameter("nr_telefone", dto.telefone));
            parms.Add(new MySqlParameter("ds_rua", dto.rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.bairro));
            parms.Add(new MySqlParameter("ds_numero_casa", dto.numero));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("dt_hora", dto.hora));
            parms.Add(new MySqlParameter("dt_data", dto.data));
            parms.Add(new MySqlParameter("ds_observacao", dto.observacao));
            parms.Add(new MySqlParameter("tb_cliente_ID_Cliente", dto.IDcliente));
            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dto.Idfuncionarios));



            DataBase db = new DataBase();
            db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM TB_Agendamento WHERE ID_Agendamento = @ID_Agendamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Agendamento", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<AgendamentoDTO> Consultar(string Nome)
        {
            string script = @"SELECT * FROM TB_Agendamento WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", Nome + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AgendamentoDTO> lista = new List<AgendamentoDTO>();
            while (reader.Read())
            {
                AgendamentoDTO dto = new AgendamentoDTO();
                dto.Id = reader.GetInt32("ID_Agendamento");
                dto.nome = reader.GetString("nm_nome");
                dto.cpf = reader.GetString("ds_cpf");
                dto.telefone = reader.GetString("nr_telefone");
                dto.rua = reader.GetString("ds_rua");
                dto.bairro = reader.GetString("ds_bairro");
                dto.numero = reader.GetString("ds_numero_casa");
                dto.cep = reader.GetString("ds_cep");
                dto.hora = reader.GetString("dt_hora");
                dto.data = reader.GetDateTime("dt_data");
                dto.observacao = reader.GetString("ds_observacao");
                dto.IDcliente = reader.GetInt32("tb_cliente_ID_Cliente");
                dto.Idfuncionarios = reader.GetInt32("tb_funcionario_id_funcionario");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<AgendamentoDTO> Listar()
        {
            string script = @"SELECT * FROM TB_Agendamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AgendamentoDTO> lista = new List<AgendamentoDTO>();
            while (reader.Read())
            {
                AgendamentoDTO dto = new AgendamentoDTO();
                dto.Id = reader.GetInt32("ID_Agendamento");
                dto.nome = reader.GetString("nm_nome");
                dto.cpf = reader.GetString("ds_cpf");
                dto.telefone = reader.GetString("nr_telefone");
                dto.rua = reader.GetString("ds_rua");
                dto.bairro = reader.GetString("ds_bairro");
                dto.numero = reader.GetString("ds_numero_casa");
                dto.cep = reader.GetString("ds_cep");
                dto.hora = reader.GetString("dt_hora");
                dto.data = reader.GetDateTime("dt_data");
                dto.observacao = reader.GetString("ds_observacao");
                dto.IDcliente = reader.GetInt32("tb_cliente_ID_Cliente");
                dto.Idfuncionarios = reader.GetInt32("tb_funcionario_id_funcionario");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public AgendamentoDTO ListarPorId(int idProduto)
        {
            string script = @"SELECT * FROM TB_Agendamento WHERE ID_Agendamento = @ID_Agendamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Agendamento", idProduto));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            AgendamentoDTO dto = new AgendamentoDTO();
            if (reader.Read())
            {
                dto.Id = reader.GetInt32("ID_Agendamento");
                dto.nome = reader.GetString("nm_nome");
                dto.cpf = reader.GetString("ds_cpf");
                dto.telefone = reader.GetString("nr_telefone");
                dto.rua = reader.GetString("ds_rua");
                dto.bairro = reader.GetString("ds_bairro");
                dto.numero = reader.GetString("ds_numero_casa");
                dto.cep = reader.GetString("ds_cep");
                dto.hora = reader.GetString("dt_hora");
                dto.data = reader.GetDateTime("dt_data");
                dto.observacao = reader.GetString("ds_observacao");
                dto.IDcliente = reader.GetInt32("tb_cliente_ID_Cliente");
                dto.Idfuncionarios = reader.GetInt32("tb_funcionario_id_funcionario");
            }
            reader.Close();
            return dto;
        }
    }
}

    


    

   
