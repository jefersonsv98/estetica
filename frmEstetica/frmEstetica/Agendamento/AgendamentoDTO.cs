﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Agendamento
{
    class AgendamentoDTO
    {
        public int Id { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public string telefone { get; set; }
        public string rua { get; set; }
        public string bairro { get; set;}
        public string numero { get; set; }
        public string cep { get; set; }
        public string hora { get; set; }
        public DateTime data { get; set; }
        public string observacao { get; set; }
        public int Idfuncionarios { get; set; }

        public int IDcliente { get; set; }

    }
}
