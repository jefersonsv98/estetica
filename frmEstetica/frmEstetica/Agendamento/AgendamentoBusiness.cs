﻿using Blibioteca.Developers.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Agendamento
{
    class AgendamentoBusiness
    {
        public int Salvar(AgendamentoDTO dto)
        {
            CPF validar = new CPF();
            validar.ValidarCPF(dto.cpf);

            AgendamentoDataBase db = new AgendamentoDataBase();
            return db.Salvar(dto);
        }

        public List<AgendamentoDTO> Consultar(string nome)
        {
            AgendamentoDataBase db = new AgendamentoDataBase();
            return db.Consultar(nome);
        }

        public void Remover(int id)
        {
            AgendamentoDataBase db = new AgendamentoDataBase();
            db.Remover(id);
        }

        public List<AgendamentoDTO> Listar()
        {
            AgendamentoDataBase db = new AgendamentoDataBase();
            return db.Listar();
        }


        public void Alterar(AgendamentoDTO dados)
        {
            AgendamentoDataBase db = new AgendamentoDataBase();
            db.Alterar(dados);
        }
        public AgendamentoDTO ListarPorId(int idProduto)
        {
            AgendamentoDataBase db = new AgendamentoDataBase();
            return db.ListarPorId(idProduto);
        }
    }
}

    

    

