﻿using Blibioteca.Developers.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            CPF validar = new CPF();
            validar.ValidarCPF(dto.cpf);

            ClienteDataBase db = new ClienteDataBase();
            return db.Salvar(dto);
        }

        public List<ClienteDTO> Consultar(string nome)
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.Consultar(nome);
        }

        public void Remover(int id)
        {
            ClienteDataBase db = new ClienteDataBase();
            db.Remover(id);
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.Listar();
        }


        public void Alterar(ClienteDTO dados)
        {
            ClienteDataBase db = new ClienteDataBase();
            db.Alterar(dados);
        }
        public ClienteDTO ListarPorId(int idProduto)
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.ListarPorId(idProduto);
        }

    }
}

    
