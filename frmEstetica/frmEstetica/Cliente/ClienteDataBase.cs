﻿using frmEstetica.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Cliente
{
    class ClienteDataBase

    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_nome,nu_rg,nu_cpf,ds_sexo,dt_nascimento,nu_telefone,nu_celular,nm_rua,nm_bairro,nu_cep,nu_casa)
                                              VALUES (@nm_nome,@nu_rg,@nu_cpf,@ds_sexo,@dt_nascimento,@nu_telefone,@nu_celular,@nm_rua,@nm_bairro,@nu_cep,@nu_casa)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.nome));
            parms.Add(new MySqlParameter("nu_rg", dto.rg));
            parms.Add(new MySqlParameter("nu_cpf", dto.cpf));
            parms.Add(new MySqlParameter("ds_sexo", dto.sexo));
            parms.Add(new MySqlParameter("dt_nascimento", dto.nascimento));
            parms.Add(new MySqlParameter("nu_telefone", dto.telefone));
            parms.Add(new MySqlParameter("nu_celular", dto.celular));
            parms.Add(new MySqlParameter("nm_rua", dto.rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.bairro));
            parms.Add(new MySqlParameter("nu_cep", dto.cep));
            parms.Add(new MySqlParameter("nu_casa", dto.numero));



            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE tb_cliente
                                 SET nm_nome = @nm_nome,
                                     nu_rg = @nu_rg,
                                     nu_cpf = @nu_rg,
                                     ds_sexo = @ds_sexo,
                                     dt_nascimento = @dt_nascimento,
                                     nu_telefone = @nu_telefone,
                                     nu_celular = @nu_celular,
                                     nm_rua = @nm_rua,
                                     nm_bairro = @nm_bairro,
                                     nu_cep = @nu_cep,
                                     nu_casa = @nu_casa
                                    WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.Id));
            parms.Add(new MySqlParameter("nm_nome", dto.nome));
            parms.Add(new MySqlParameter("nu_rg", dto.rg));
            parms.Add(new MySqlParameter("nu_cpf", dto.cpf));
            parms.Add(new MySqlParameter("ds_sexo", dto.sexo));
            parms.Add(new MySqlParameter("dt_nascimento", dto.nascimento));
            parms.Add(new MySqlParameter("nu_telefone", dto.telefone));
            parms.Add(new MySqlParameter("nu_celular", dto.celular));
            parms.Add(new MySqlParameter("nm_rua", dto.rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.bairro));
            parms.Add(new MySqlParameter("nu_cep", dto.cep));
            parms.Add(new MySqlParameter("nu_casa", dto.numero));


            DataBase db = new DataBase();
            db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ClienteDTO> Consultar(string Nome)
        {
            string script = @"SELECT * FROM tb_cliente WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", Nome + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.nome = reader.GetString("nm_nome");
                dto.rg = reader.GetString("nu_rg");
                dto.cpf = reader.GetString("nu_cpf");
                dto.sexo = reader.GetString("ds_sexo");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.telefone = reader.GetString("nu_telefone");
                dto.celular = reader.GetString("nu_celular");
                dto.rua = reader.GetString("nm_rua");
                dto.bairro = reader.GetString("nm_bairro");
                dto.cep = reader.GetString("nu_cep");
                dto.numero = reader.GetString("nu_casa");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();

                dto.Id = reader.GetInt32("id_cliente");
                dto.nome = reader.GetString("nm_nome");
                dto.rg = reader.GetString("nu_rg");
                dto.cpf = reader.GetString("nu_cpf");
                dto.sexo = reader.GetString("ds_sexo");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.telefone = reader.GetString("nu_telefone");
                dto.celular = reader.GetString("nu_celular");
                dto.rua = reader.GetString("nm_rua");
                dto.bairro = reader.GetString("nm_bairro");
                dto.cep = reader.GetString("nu_cep");
                dto.numero = reader.GetString("nu_casa");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public ClienteDTO ListarPorId(int idProduto)
        {
            string script = @"SELECT * FROM tb_cliente WHERE ID_Cliente = @ID_Cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Cliente", idProduto));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ClienteDTO dto = new ClienteDTO();
            if (reader.Read())
            {
                dto.Id = reader.GetInt32("ID_Cliente");
                dto.nome = reader.GetString("nm_nome");
                dto.rg = reader.GetString("nu_rg");
                dto.cpf = reader.GetString("nu_cpf");
                dto.sexo = reader.GetString("ds_sexo");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.telefone = reader.GetString("nu_telefone");
                dto.celular = reader.GetString("nu_celular");
                dto.rua = reader.GetString("nm_rua");
                dto.bairro = reader.GetString("nm_bairro");
                dto.cep = reader.GetString("nu_cep");
                dto.numero = reader.GetString("nu_casa");
            }
            reader.Close();
            return dto;
        }
    }
}

    


    

