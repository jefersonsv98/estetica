﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Cliente
{
    class ClienteDTO
    {
        public int Id { get; set; }
        public string nome { get; set; }
        public string rg { get; set; }
        public string cpf { get; set; }
        public string sexo { get; set; }
        public DateTime nascimento { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string rua { get; set; }
        public string bairro { get; set; }
        public string cep { get; set; }
        public string numero { get; set; }

    }
}
