﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Folha_de_Pagamento
{
    class PagamentoBusiness
    {
        public int Salvar(PagamentoDTO dto)
        {
            PagamentoDataBase db = new PagamentoDataBase();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            PagamentoDataBase db = new PagamentoDataBase();
            db.Remover(id);
        }

        public void Alterar(PagamentoDTO dados)
        {
            PagamentoDataBase db = new PagamentoDataBase();
            db.Alterar(dados);
        }

        public List<PagamentoConsultarView> Consultar(string Nome)
        {
            PagamentoDataBase db = new PagamentoDataBase();
            return db.Consultar(Nome);
        }

        public List<PagamentoDTO> Listar()
        {
            PagamentoDataBase db = new PagamentoDataBase();
            return db.Listar();
        }
    }
}

