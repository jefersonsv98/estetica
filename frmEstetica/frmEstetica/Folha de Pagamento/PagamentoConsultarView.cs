﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Folha_de_Pagamento
{
    class PagamentoConsultarView
    {
        public int Id { get; set; }
        public DateTime data { get; set; }
        public string horames { get; set; }
        public string salariohora { get; set; }
        public string valor { get; set; }
        public string aliquota { get; set; }
        public string inss { get; set; }
        public string salariofinal { get; set; }
        public string horaext50 { get; set; }
        public string horaext100 { get; set; }
        public string vlrefeicao { get; set; }
        public string vltransporte { get; set; }
        public string Funcionario { get; set; }
    }
}
