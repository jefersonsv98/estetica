﻿using frmEstetica.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Folha_de_Pagamento
{
    class PagamentoDataBase
    {
        public int Salvar(PagamentoDTO dados)
        {
            string script =
             @"INSERT INTO tb_folha_pagamento (dt_pagamento, nu_hora_mes, vl_salario_hora, vl_valor, vl_aliquota,  vl_salario_final,  vl_inss,  ds_hora_extra1, ds_hora_extra2, vl_refeicao,  vl_transporte, tb_funcionario_id_funcionario)
                                       VALUES (@dt_pagamento, @nu_hora_mes, @vl_salario_hora, @vl_valor, @vl_aliquota,  @vl_salario_final,  @vl_inss,  @ds_hora_extra1, @ds_hora_extra2, @vl_refeicao,  @vl_transporte, @tb_funcionario_id_funcionario)";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dados.FuncionarioId));
            parms.Add(new MySqlParameter("dt_pagamento", dados.data));
            parms.Add(new MySqlParameter("nu_hora_mes", dados.horames));
            parms.Add(new MySqlParameter("vl_salario_hora", dados.salariohora));
            parms.Add(new MySqlParameter("vl_valor", dados.valor));
            parms.Add(new MySqlParameter("vl_aliquota", dados.aliquota));
            parms.Add(new MySqlParameter("vl_salario_final", dados.salariofinal));
            parms.Add(new MySqlParameter("vl_inss", dados.inss));
            parms.Add(new MySqlParameter("ds_hora_extra1", dados.horaext50));
            parms.Add(new MySqlParameter("ds_hora_extra2", dados.horaext100));
            parms.Add(new MySqlParameter("vl_refeicao", dados.vlrefeicao));
            parms.Add(new MySqlParameter("vl_transporte", dados.vltransporte));


            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<PagamentoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_folha_pagamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PagamentoDTO> lista = new List<PagamentoDTO>();
            while (reader.Read())
            {
                PagamentoDTO dados = new PagamentoDTO();
                parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dados.FuncionarioId));
                parms.Add(new MySqlParameter("dt_pagamento", dados.data));
                parms.Add(new MySqlParameter("nu_hora_mes", dados.horames));
                parms.Add(new MySqlParameter("vl_salario_hora", dados.salariohora));
                parms.Add(new MySqlParameter("vl_valor", dados.valor));
                parms.Add(new MySqlParameter("vl_aliquota", dados.aliquota));
                parms.Add(new MySqlParameter("vl_salario_final", dados.salariofinal));
                parms.Add(new MySqlParameter("vl_inss", dados.inss));
                parms.Add(new MySqlParameter("ds_hora_extra1", dados.horaext50));
                parms.Add(new MySqlParameter("ds_hora_extra2", dados.horaext100));
                parms.Add(new MySqlParameter("vl_refeicao", dados.vlrefeicao));
                parms.Add(new MySqlParameter("vl_transporte", dados.vltransporte));

                lista.Add(dados);
            }
            reader.Close();

            return lista;
        }

        public List<PagamentoConsultarView> Consultar(string Nome)
        {
            string script = @"SELECT * FROM vw_folha_pagamento_consultar WHERE nm_funcionario like @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", Nome + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PagamentoConsultarView> lista = new List<PagamentoConsultarView>();
            while (reader.Read())
            {
                PagamentoConsultarView dto = new PagamentoConsultarView();
                dto.Id = reader.GetInt32("id_folha_pagamento");
                dto.data = reader.GetDateTime("dt_pagamento");
                dto.horames = reader.GetString("nu_hora_mes");
                dto.salariohora = reader.GetString("vl_salario_hora");
                dto.valor = reader.GetString("vl_valor");
                dto.aliquota = reader.GetString("vl_aliquota");
                dto.inss = reader.GetString("vl_inss");
                dto.salariofinal = reader.GetString("vl_salario_final");
                dto.horaext50 = reader.GetString("ds_hora_extra1");
                dto.horaext100 = reader.GetString("ds_hora_extra2");
                dto.vlrefeicao = reader.GetString("vl_refeicao");
                dto.vltransporte = reader.GetString("vl_transporte");
                dto.Funcionario = reader.GetString("nm_funcionario");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_folha_pagamento WHERE id_folha_pagamento = @id_folha_pagamanto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folha_pagamanto", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(PagamentoDTO dados)
        {
            string script = @"UPDATE tb_folha_pagamento 
                                 SET tb_funcionario_id_funcionario = @tb_funcionario_id_funcionario,
                                     dt_pagamento = @dt_pagamento,
                                     nu_hora_mes = @nu_hora_mes,
                                     vl_salario_hora = @vl_salario_hora,
                                     vl_valor = @vl_valor,
                                     vl_aliquota = @vl_aliquota,
                                     vl_salario_final = @vl_salario_final,
                                     vl_inss = @vl_inss,
                                     ds_hora_extra1 = @ds_hora_extra1,
                                     ds_hora_extra2 = @ds_hora_extra2,
                                     vl_refeicao = @vl_refeicao,
                                     vl_transporte = @vl_transporte
                               WHERE id_folha_pagamento = @id_folha_pagamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folha_pagamento", dados.Id));
            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dados.FuncionarioId));
            parms.Add(new MySqlParameter("dt_pagamento", dados.data));
            parms.Add(new MySqlParameter("nu_hora_mes", dados.horames));
            parms.Add(new MySqlParameter("vl_salario_hora", dados.salariohora));
            parms.Add(new MySqlParameter("vl_valor", dados.valor));
            parms.Add(new MySqlParameter("vl_aliquota", dados.aliquota));
            parms.Add(new MySqlParameter("vl_salario_final", dados.salariofinal));
            parms.Add(new MySqlParameter("vl_inss", dados.inss));
            parms.Add(new MySqlParameter("ds_hora_extra1", dados.horaext50));
            parms.Add(new MySqlParameter("ds_hora_extra2", dados.horaext100));
            parms.Add(new MySqlParameter("vl_refeicao", dados.vlrefeicao));
            parms.Add(new MySqlParameter("vl_transporte", dados.vltransporte));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);



        }
    }
}