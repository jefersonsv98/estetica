﻿using frmEstetica.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Estoque
{
    class EstoqueDataBase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script = @"INSERT INTO tb_estoque (tb_funcionario_id_funcionario,tb_produto_id_produto,qtd_produto)
                                              VALUES (@tb_funcionario_id_funcionario,@tb_produto_id_produto,@qtd_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dto.Idfuncionario));
            parms.Add(new MySqlParameter("tb_produto_id_produto", dto.Idproduto));
            parms.Add(new MySqlParameter("qtd_produto", dto.quantidade));




            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque
                                 SET tb_funcionario_id_funcionario = @tb_funcionario_id_funcionario,
                                     tb_produto_id_produto = @tb_produto_id_produto,
                                     qtd_produto = @qtd_produto
                                    WHERE ID_Estoque = @ID_Estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Estoque", dto.Id));
            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dto.Idfuncionario));
            parms.Add(new MySqlParameter("tb_produto_id_produto", dto.Idproduto));
            parms.Add(new MySqlParameter("qtd_produto", dto.quantidade));


            DataBase db = new DataBase();
            db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_estoque WHERE ID_Estoque = @ID_Estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Estoque", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<EstoqueDTO> Consultar(string Nome)
        {
            string script = @"SELECT * FROM tb_estoque WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", Nome + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.Id = reader.GetInt32("ID_Estoque");
                dto.Idfuncionario = reader.GetInt32("tb_funcionario_id_funcionario");
                dto.Idproduto = reader.GetInt32("tb_produto_id_produto");
                dto.quantidade = reader.GetInt32("qtd_produto");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<EstoqueDTO> Listar()
        {
            string script = @"SELECT * FROM tb_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();

                dto.Id = reader.GetInt32("ID_Estoque");
                dto.Idfuncionario = reader.GetInt32("tb_funcionario_id_funcionario");
                dto.Idproduto = reader.GetInt32("tb_produto_id_produto");
                dto.quantidade = reader.GetInt32("qtd_produto");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

    


    

    

