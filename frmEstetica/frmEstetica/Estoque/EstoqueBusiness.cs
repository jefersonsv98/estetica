﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Estoque
{
    class EstoqueBusiness
    {
        public int Salvar(EstoqueDTO dto)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            return db.Salvar(dto);
        }

        public List<EstoqueDTO> Consultar(string nome)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            return db.Consultar(nome);
        }

        public void Remover(int id)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            db.Remover(id);
        }

        public List<EstoqueDTO> Listar()
        {
            EstoqueDataBase db = new EstoqueDataBase();
            return db.Listar();
        }


        public void Alterar(EstoqueDTO dados)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            db.Alterar(dados);
        }
    
    }
}
    

