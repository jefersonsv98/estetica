﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Estoque
{
    class EstoqueDTO
    {
        public int Id { get; set; }
        public int quantidade { get; set; }
        public int Idfuncionario { get; set; }
        public int Idproduto { get; set; }
    }
}
