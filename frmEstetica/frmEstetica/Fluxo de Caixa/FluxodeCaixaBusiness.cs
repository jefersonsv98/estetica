﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Fluxo_de_Caixa
{
    class FluxodeCaixaBusiness
    {


        public List<FluxodeCaixaDTO> Listar()
        {
            FluxodeCaixaDataBase db = new FluxodeCaixaDataBase();
            return db.Listar();
        }

        public FluxodeCaixaConsultarView ListarPorId(int Id)
        {
            FluxodeCaixaDataBase db = new FluxodeCaixaDataBase();
            return db.ListarPorId(Id);
        }

        public List<FluxoCaixaDTO2> ListarVenda()
        {
            FluxodeCaixaDataBase db = new FluxodeCaixaDataBase();
            return db.ListarVenda();
        }

        public FluxoCaixaConsultarView2 ListarPorIdVenda(int Id)
        {
            FluxodeCaixaDataBase db = new FluxodeCaixaDataBase();
            return db.ListarPorIdVenda(Id);
        }
    }
        
}




