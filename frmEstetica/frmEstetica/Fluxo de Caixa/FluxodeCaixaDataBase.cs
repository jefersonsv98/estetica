﻿using frmEstetica.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Fluxo_de_Caixa
{
    class FluxodeCaixaDataBase
    {

        public FluxodeCaixaConsultarView ListarPorId(int Id)
        {
            string script = @"SELECT * FROM vw_fluxo_caixa_consultar WHERE id_fluxo_caixa like @id_fluxo_caixa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fluxo_caixa", Id));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FluxodeCaixaConsultarView dto = new FluxodeCaixaConsultarView();
            if (reader.Read())
            {
                dto.Id = reader.GetInt32("id_fluxo_caixa");
                dto.Pagamento = reader.GetString("ROUND(sum(vl_salario_final),2)");
                dto.horaextra1 = reader.GetString("ROUND(sum(ds_hora_extra1),2)");
                dto.horaextra2 = reader.GetString("ROUND(sum(ds_hora_extra2),2)");
                dto.refeicao = reader.GetString("ROUND(sum(vl_refeicao),2)");
                dto.transporte = reader.GetString("ROUND(sum(vl_transporte),2)");
            }
            reader.Close();
            return dto;
        }

        public FluxoCaixaConsultarView2 ListarPorIdVenda(int Id)
        {
            string script = @"SELECT * FROM vw_fluxo_caixa_consultar_pedido WHERE id_fluxo_caixa like @id_fluxo_caixa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fluxo_caixa", Id));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FluxoCaixaConsultarView2 dto = new FluxoCaixaConsultarView2();
            if (reader.Read())
            {
                dto.Id = reader.GetInt32("id_fluxo_caixa");
                dto.Venda = reader.GetString("ROUND(sum(vl_valortotal),2)");
            }
            reader.Close();
            return dto;
        }

        public List<FluxodeCaixaDTO> Listar()
        {
            string script = @"SELECT * FROM vw_fluxo_caixa_consultar";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FluxodeCaixaDTO> lista = new List<FluxodeCaixaDTO>();
            while (reader.Read())
            {
                FluxodeCaixaDTO dto = new FluxodeCaixaDTO();

                dto.Id = reader.GetInt32("id_fluxo_caixa");
                dto.Pagamento = reader.GetString("ROUND(sum(vl_salario_final),2)");
                dto.horaextra1 = reader.GetString("ROUND(sum(ds_hora_extra1),2)");
                dto.horaextra2 = reader.GetString("ROUND(sum(ds_hora_extra2),2)");
                dto.refeicao = reader.GetString("ROUND(sum(vl_refeicao),2)");
                dto.transporte = reader.GetString("ROUND(sum(vl_transporte),2)");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<FluxoCaixaDTO2> ListarVenda()
        {
            string script = @"SELECT * FROM vw_fluxo_caixa_consultar_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FluxoCaixaDTO2> lista = new List<FluxoCaixaDTO2>();
            while (reader.Read())
            {
                FluxoCaixaDTO2 dto = new FluxoCaixaDTO2();

                dto.Id = reader.GetInt32("id_fluxo_caixa");
                dto.Venda = reader.GetString("ROUND(sum(vl_valortotal),2)");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
