﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Fluxo_de_Caixa
{
    class FluxodeCaixaDTO
    {
        public int Id { get; set; }
        public string Pagamento { get; set; }
        public string horaextra1 { get; set; }
        public string horaextra2 { get; set; }
        public string transporte { get; set; }
        public string refeicao { get; set; }
    }
}
