﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmEstetica
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(3500);
                Invoke(new Action(() =>
                {
                    frmLogar frm = new frmLogar();
                    frm.Show();
                    Hide();
                }));
            });
    }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
