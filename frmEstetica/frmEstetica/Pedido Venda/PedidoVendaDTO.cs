﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Pedido_Venda
{
    class PedidoVendaDTO
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int FuncionarioId { get; set; }
        public int ProdutoId { get; set; }
        public int quantidade { get; set; }
        public string pagamento { get; set; }
        public DateTime data { get; set; }
        public decimal valorfinal { get; set; }
        public decimal valortotal { get; set; }

    }
}
