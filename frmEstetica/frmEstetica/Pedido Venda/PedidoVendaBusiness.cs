﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Pedido_Venda
{
    class PedidoVendaBusiness
    {
        public int Salvar(PedidoVendaDTO dto)
        {
            PedidoVendaDataBase db = new PedidoVendaDataBase();
            return db.Salvar(dto);
        }

        public List<PedidoVendaConsultarView> Consultar(string nome)
        {
            PedidoVendaDataBase db = new PedidoVendaDataBase();
            return db.Consultar(nome);
        }

        public void Remover(int Id)
        {
            PedidoVendaDataBase db = new PedidoVendaDataBase();
            db.Remover(Id);
        }

        public List<PedidoVendaDTO> Listar()
        {
            PedidoVendaDataBase db = new PedidoVendaDataBase();
            return db.Listar();
        }

        public void Alterar(PedidoVendaDTO dados)
        {
            PedidoVendaDataBase db = new PedidoVendaDataBase();
            db.Alterar(dados);
        }
        public PedidoVendaDTO ListarPorId(int idVenda)
        {
            PedidoVendaDataBase db = new PedidoVendaDataBase();
            return db.ListarPorId(idVenda);
        }   

    }
}
