﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Pedido_Venda
{
    class PedidoVendaConsultarView
    {
        public int Id { get; set; }
        public string Cliente { get; set; }
        public string Funcionario { get; set; }
        public string Produto { get; set; }
        public int quantidade { get; set; }
        public string pagamento { get; set; }
        public DateTime data { get; set; }
        public decimal valorfinal { get; set; }
        public decimal valortotal { get; set; }
    }
}
