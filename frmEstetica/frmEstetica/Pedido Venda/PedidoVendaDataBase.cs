﻿using frmEstetica.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmEstetica.Pedido_Venda
{
    class PedidoVendaDataBase
    {
        public int Salvar(PedidoVendaDTO dados)
        {
            string script =
              @"INSERT INTO tb_pedido (fm_pagamento, qtd_produto, dt_data, vl_pago, tb_funcionario_id_funcionario, tb_produto_id_produto, tb_cliente_ID_Cliente, vl_valortotal)
                              VALUES (@fm_pagamento, @qtd_produto, @dt_data, @vl_pago, @tb_funcionario_id_funcionario, @tb_produto_id_produto, @tb_cliente_ID_Cliente, @vl_valortotal)";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("fm_pagamento", dados.pagamento));
            parms.Add(new MySqlParameter("qtd_produto", dados.quantidade));
            parms.Add(new MySqlParameter("dt_data", dados.data));
            parms.Add(new MySqlParameter("vl_pago", dados.valorfinal));
            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dados.FuncionarioId));
            parms.Add(new MySqlParameter("tb_produto_id_produto", dados.ProdutoId));
            parms.Add(new MySqlParameter("tb_cliente_ID_Cliente", dados.ClienteId));
            parms.Add(new MySqlParameter("vl_valortotal", dados.valortotal));

            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<PedidoVendaDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoVendaDTO> lista = new List<PedidoVendaDTO>();
            while (reader.Read())
            {
                PedidoVendaDTO dados = new PedidoVendaDTO();
                parms.Add(new MySqlParameter("id_pedido", dados.Id));
                parms.Add(new MySqlParameter("fm_pagamento", dados.pagamento));
                parms.Add(new MySqlParameter("qtd_produto", dados.quantidade));
                parms.Add(new MySqlParameter("dt_data", dados.data));
                parms.Add(new MySqlParameter("vl_pago", dados.pagamento));
                parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dados.FuncionarioId));
                parms.Add(new MySqlParameter("tb_produto_id_produto", dados.ProdutoId));
                parms.Add(new MySqlParameter("tb_cliente_ID_Cliente", dados.ClienteId));
                lista.Add(dados);
            }
            reader.Close();

            return lista;
        }

        public List<PedidoVendaConsultarView> Consultar(string Nome)
        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", Nome + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoVendaConsultarView> lista = new List<PedidoVendaConsultarView>();
            while (reader.Read())
            {
                PedidoVendaConsultarView dto = new PedidoVendaConsultarView();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Cliente = reader.GetString("nm_nome");
                dto.Funcionario = reader.GetString("nm_funcionario");
                dto.Produto = reader.GetString("ds_produto");
                dto.quantidade = reader.GetInt32("qtd_produto");
                dto.pagamento = reader.GetString("fm_pagamento");
                dto.data = reader.GetDateTime("dt_data");
                dto.valortotal = reader.GetDecimal("vl_valortotal");
                dto.valorfinal = reader.GetDecimal("vl_pago");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public void Remover(int Id)
        {
            string script = @"DELETE FROM tb_pedido WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", Id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(PedidoVendaDTO dados)
        {
            string script = @"UPDATE tb_pedido 
                                 SET fm_pagamento = @fm_pagamento,
                                     qtd_produto = @qtd_produto,
                                     dt_data = @dt_data,
                                     vl_pago = @vl_pago,
                                     tb_funcionario_id_funcionario = @tb_funcionario_id_funcionario,
                                     tb_produto_id_produto = @tb_produto_id_produto,
                                     tb_cliente_ID_Cliente = @tb_cliente_ID_Cliente,
                                     vl_valortotal = @vl_valortotal
                               WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", dados.Id));
            parms.Add(new MySqlParameter("fm_pagamento", dados.pagamento));
            parms.Add(new MySqlParameter("qtd_produto", dados.quantidade));
            parms.Add(new MySqlParameter("dt_data", dados.data));
            parms.Add(new MySqlParameter("vl_pago", dados.valorfinal));
            parms.Add(new MySqlParameter("tb_funcionario_id_funcionario", dados.FuncionarioId));
            parms.Add(new MySqlParameter("tb_produto_id_produto", dados.ProdutoId));
            parms.Add(new MySqlParameter("tb_cliente_ID_Cliente", dados.ClienteId));
            parms.Add(new MySqlParameter("vl_valortotal", dados.valortotal));
            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }
        public PedidoVendaDTO ListarPorId(int idVenda)
        {
            string script = @"SELECT * FROM tb_pedido WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", idVenda));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            PedidoVendaDTO dto = new PedidoVendaDTO();
            if (reader.Read())
            {
                dto.Id = reader.GetInt32("id_pedido");
                dto.ClienteId = reader.GetInt32("tb_cliente_ID_Cliente");
                dto.FuncionarioId = reader.GetInt32("tb_funcionario_id_funcionario");
                dto.ProdutoId = reader.GetInt32("tb_produto_id_produto");
                dto.quantidade = reader.GetInt32("qtd_produto");
                dto.pagamento = reader.GetString("fm_pagamento");
                dto.data = reader.GetDateTime("dt_data");
                dto.valortotal = reader.GetDecimal("vl_valortotal");
                dto.valorfinal = reader.GetDecimal("vl_pago");
            }
            reader.Close();
            return dto;
        }

    }
}
